const proxy = require('express-http-proxy')
const app = require('express')()

app.use('/', proxy('localhost:3000', { filter: function (req) { return req.headers.host === 'app-fe' } }))
app.use('/', proxy('localhost:4000', { filter: function (req) { return req.headers.host === 'app-gw' } }))
app.use('/', proxy('localhost:4002', { filter: function (req) { return req.headers.host === 'service-auth' } }))
app.use('/', proxy('localhost:4003', { filter: function (req) { return req.headers.host === 'service-sales' } }))
app.use('/', proxy('localhost:4004', { filter: function (req) { return req.headers.host === 'service-workout' } }))

app.listen(80)
