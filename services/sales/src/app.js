import express from 'express'
import bodyParser from 'body-parser'

import response from './helpers/response'

import BalanceRouter from './endpoints/balance'
import SubscriptionsRouter from './endpoints/subscriptions'
import OrdersRouter from './endpoints/orders'
import ProductsRouter from './endpoints/products'
import ServicesRouter from './endpoints/service'

const addEndpoints = (app) => {
  app.get('/', (req, res) => { res.send('ok1231') })
  
  app.use(BalanceRouter)
  app.use(SubscriptionsRouter)
  app.use(OrdersRouter)
  app.use(ProductsRouter)
  app.use(ServicesRouter)
}

const app = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json({ limit: '2mb' }))

addEndpoints(app)

app.use('*', function (req, res) {
  response.error(res, 'UnknownPath', 404)
})

app.use((err, req, res, next) => {
  console.log('t',req.url)
  if (res.headersSent) {
    return next()
  }
  console.log('err', err)
  response.error(res, 'InternalError', 500, err)
})

export default app
