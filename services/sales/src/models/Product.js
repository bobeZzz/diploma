import mongoose from 'mongoose'
import { v1 as uuid } from 'uuid'

const ProductSchema = new mongoose.Schema({
  uuid: { type: String, required: true, default: uuid },
  name: { type: String, required: true },
  description: { type: String },
  price: { type: Number, required: true },
  photoUrl: { type: String, default: '' },
  amount: { type: Number, required: true, default: 0 }
})

ProductSchema.index({ uuid: 1 }, { name: 'uuidIndex', unique: true })

ProductSchema.methods.toJSON = function () {
  var obj = this.toObject()

  delete obj._id
  delete obj.__v

  return obj
}

export default mongoose.model('Product', ProductSchema)
