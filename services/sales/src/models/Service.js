import mongoose from 'mongoose'
import { v1 as uuid } from 'uuid'

const ServiceSchema = new mongoose.Schema({
  uuid: { type: String, required: true, default: uuid },
  name: { type: String, required: true },
  type: { type: String, required: true },
  price: { type: Number, required: true }
})

ServiceSchema.index({ uuid: 1 }, { name: 'uuidIndex', unique: true })
ServiceSchema.index({ type: 1 }, { name: 'typeIndex', unique: true })

ServiceSchema.methods.toJSON = function () {
  var obj = this.toObject()

  delete obj._id
  delete obj.__v

  return obj
}

export default mongoose.model('Service', ServiceSchema)
