import express from 'express'

import GetBalance from './GetBalance'
import UpdateBalance from './UpdateBalance'

const router = express.Router()

router.use(GetBalance)
router.use(UpdateBalance)

export default router
