import express from 'express'

import response from '../../helpers/response'
import { getUserBalance } from '../../helpers/common'

const GetBalance = express.Router()

GetBalance.get('/balance',
  async (req, res, next) => {
    try {
      const { userId } = req.query

      const balance = await getUserBalance(userId)

      response.ok(res, balance)
    } catch (e) {
      console.log('GetBalance e', e)
      next(e)
    }
  }
)

export default GetBalance
