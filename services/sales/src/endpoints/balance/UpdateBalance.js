import express from 'express'

import response from '../../helpers/response'
import { getUserBalance } from '../../helpers/common'

const UpdateBalance = express.Router()

UpdateBalance.patch('/balance/:userId',
  async (req, res, next) => {
    try {
      const { amount = 0, personalTrainings = 0 } = req.body
      const { userId } = req.params

      const balance = await getUserBalance(userId)

      balance.balance = (+(balance.balance + +amount)).toFixed(2)
      balance.personalTrainings = +(balance.personalTrainings + +personalTrainings)
      await balance.save()

      response.ok(res, balance, 201)
    } catch (e) {
      console.log('UpdateBalance e', e)
      next(e)
    }
  }
)

export default UpdateBalance
