import express from 'express'

import response from '../../helpers/response'
import ProductModel from '../../models/Product'

const Create = express.Router()

Create.post('/products',
  async (req, res, next) => {
    try {
      const product = await ProductModel.create(req.body)

      response.ok(res, product, 201)
    } catch (e) {
      console.log('CreateProducts e', e)
      next(e)
    }
  }
)

export default Create
