import express from 'express'

import CreateProduct from './create'
import DeleteProduct from './delete'
import GetProducts from './get'
import UpdateProduct from './update'

const router = express.Router()

router.use(CreateProduct)
router.use(DeleteProduct)
router.use(GetProducts)
router.use(UpdateProduct)

export default router
