import express from 'express'

import response from '../../helpers/response'
import ProductModel from '../../models/Product'

const Delete = express.Router()

Delete.delete('/products',
  async (req, res, next) => {
    try {
      const product = await ProductModel.findOneAndDelete({ uuid: req.query.uuid })

      response.ok(res, product)
    } catch (e) {
      console.log('DeleteProduct e', e)
      next(e)
    }
  }
)

export default Delete
