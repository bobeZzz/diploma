import express from 'express'

import response from '../../helpers/response'
import ProductModel from '../../models/Product'

const Update = express.Router()

Update.patch('/products/:uuid',
  async (req, res, next) => {
    try {
      const { uuid } = req.params
      
      const product = await ProductModel.findOneAndUpdate({ uuid }, req.body, { new: true })
      
      response.ok(res, product, 201)
    } catch (e) {
      console.log('UpdateProduct e', e)
      next(e)
    }
  }
)

export default Update
