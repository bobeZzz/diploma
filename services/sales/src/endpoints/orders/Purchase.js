import express from 'express'

import response from '../../helpers/response'
import OrderModel from '../../models/Order'

const Purchase = express.Router()

Purchase.post('/purchase',
  async (req, res, next) => {
    try {
      const order = await OrderModel.create(req.body)

      response.ok(res, order, 201)
    } catch (e) {
      console.log('Purchase e', e)
      next(e)
    }
  }
)

export default Purchase
