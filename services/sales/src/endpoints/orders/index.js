import express from 'express'

import Purchase from './Purchase'
import Get from './Get'

const router = express.Router()

router.use(Purchase)
router.use(Get)

export default router
