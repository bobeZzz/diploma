import express from 'express'

import response from '../../helpers/response'
import SubscriptionModel from '../../models/Subscription'

const CreateSubscription = express.Router()

CreateSubscription.post('/subscriptions',
  async (req, res, next) => {
    try {
      const subscription = await SubscriptionModel.create(req.body)

      response.ok(res, subscription, 201)
    } catch (e) {
      console.log('CreateSubscription e', e)
      next(e)
    }
  }
)

export default CreateSubscription
