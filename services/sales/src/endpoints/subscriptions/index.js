import express from 'express'

import CreateSubscription from './CreateSubscription'
import DeleteSubscription from './DeleteSubscription'
import GetSubscriptions from './GetSubscriptions'
import UpdateSubscription from './UpdateSubscription'

const router = express.Router()

router.use(CreateSubscription)
router.use(DeleteSubscription)
router.use(GetSubscriptions)
router.use(UpdateSubscription)

export default router
