import express from 'express'

import response from '../../helpers/response'
import ServiceModel from '../../models/Service'

const Create = express.Router()

Create.post('/services',
  async (req, res, next) => {
    try {
      const service = await ServiceModel.create(req.body)

      response.ok(res, service, 201)
    } catch (e) {
      console.log('CreateServices e', e)
      next(e)
    }
  }
)

export default Create
