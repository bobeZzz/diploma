import express from 'express'
import bodyParser from 'body-parser'

import response from './helpers/response'

import UserSubscriptions from './endpoints/UserSubscriptions'
import TrainingEvents from './endpoints/TrainingEvents'
import GymSessions from './endpoints/GymSessions'

const addEndpoints = (app) => {
  app.get('/', (req, res) => { res.send('ok1231') })
  
  app.use(UserSubscriptions)
  app.use(TrainingEvents)
  app.use(GymSessions)
}

const app = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json({ limit: '2mb' }))

addEndpoints(app)

app.use('*', function (req, res) {
  response.error(res, 'UnknownPath', 404)
})

app.use((err, req, res, next) => {
  if (res.headersSent) {
    return next()
  }
  console.log('err', err)
  response.error(res, 'InternalError', 500, err)
})

export default app
