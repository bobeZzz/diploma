import express from 'express'

import response from '../../helpers/response'
import UserSubscriptions from '../../models/UserSubscriptions'

const UpdateUserSubscription = express.Router()

UpdateUserSubscription.patch('/userSubscriptions/:uuid',
  async (req, res, next) => {
    try {
      const subscription = await UserSubscriptions.findOneAndUpdate(
        { uuid: req.params.uuid },
        req.body,
        { new: true }
      )
      
      response.ok(res, subscription)
    } catch (e) {
      console.log('UpdateUserSubscription e', e)
      next(e)
    }
  }
)

export default UpdateUserSubscription
