import express from 'express'

import response from '../../helpers/response'
import UserSubscriptions from '../../models/UserSubscriptions'

const GetUserSubscription = express.Router()

GetUserSubscription.get('/userSubscriptions',
  async (req, res, next) => {
    try {
      const subscriptions = await UserSubscriptions.find(req.query)

      response.ok(res, subscriptions)
    } catch (e) {
      console.log('GetUserSubscription e', e)
      next(e)
    }
  }
)

export default GetUserSubscription
