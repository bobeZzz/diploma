import express from 'express'

import AddUserSubscription from './AddUserSubscription'
import GetUserSubscription from './GetUserSubscription'
import UpdateUserSubscription from './UpdateUserSubscription'

const router = express.Router()

router.use(AddUserSubscription)
router.use(GetUserSubscription)
router.use(UpdateUserSubscription)

export default router
