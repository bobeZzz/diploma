import express from 'express'

import response from '../../helpers/response'
import TrainingEvent from '../../models/TrainingEvent'

const Get = express.Router()

Get.get('/trainingEvents',
  async (req, res, next) => {
    try {
      const trainingEvents = await TrainingEvent.find(req.query)

      response.ok(res, trainingEvents)
    } catch (e) {
      console.log('TrainingEvent get e', e)
      next(e)
    }
  }
)

export default Get
