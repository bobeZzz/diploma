import express from 'express'

import response from '../../helpers/response'
import TrainingEvent from '../../models/TrainingEvent'

const Create = express.Router()

Create.post('/trainingEvents',
  async (req, res, next) => {
    try {
      const trainingEvent = await TrainingEvent.create(req.body)

      response.ok(res, trainingEvent)
    } catch (e) {
      console.log('TrainingEvent create e', e)
      next(e)
    }
  }
)

export default Create
