import express from 'express'

import response from '../../helpers/response'
import GymSession from '../../models/GymSession'

const Get = express.Router()

Get.get('/gymSessions',
  async (req, res, next) => {
    try {
      const session = await GymSession.find(req.query)

      response.ok(res, session)
    } catch (e) {
      console.log('GymSession get e', e)
      next(e)
    }
  }
)

export default Get
