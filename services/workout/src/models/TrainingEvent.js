import mongoose from 'mongoose'
import { v1 as uuid } from 'uuid'

const TrainingEventSchema = new mongoose.Schema({
  uuid: { type: String, required: true, default: uuid },
  type: { type: String, required: true, default: 'solo', enum: ['solo', 'duo', 'group'] },

  start: { type: Date, required: true },
  end: { type: Date, required: true },

  userId: { type: String, required: true },
  
  coachId: { type: String, default: null },
  declinedBy: { type: String, default: null }
})

TrainingEventSchema.index({ uuid: 1 }, { name: 'uuidIndex', unique: true })

TrainingEventSchema.methods.toJSON = function () {
  var obj = this.toObject()

  delete obj._id
  delete obj.__v

  return obj
}

export default mongoose.model('TrainingEvent', TrainingEventSchema)
