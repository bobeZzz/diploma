import mongoose from 'mongoose'
import { v1 as uuid } from 'uuid'

const UserSubscriptionSchema = new mongoose.Schema({
  uuid: { type: String, required: true, default: uuid },
  userId: { type: String, required: true },
  subscriptionId: { type: String, required: true },
  startDate: { type: String, required: true, default: Date.now },
  endDate: { type: String, required: true },
  pauseDate: { type: String },
  isPaused: { type: Boolean, required: true, default: false }
  // on unpause increase endDate
})

UserSubscriptionSchema.index({ uuid: 1 }, { name: 'uuidIndex', unique: true })

UserSubscriptionSchema.methods.toJSON = function () {
  var obj = this.toObject()

  delete obj._id
  delete obj.__v

  return obj
}

export default mongoose.model('UserSubscription', UserSubscriptionSchema)
