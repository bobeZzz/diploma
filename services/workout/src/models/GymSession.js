import mongoose from 'mongoose'
import { v1 as uuid } from 'uuid'

const GymSessionSchema = new mongoose.Schema({
  uuid: { type: String, required: true, default: uuid },
  
  userId: { type: String, required: true },

  startDate: { type: Date, required: true, default: Date.now },
  endDate: { type: Date, default: null },

  locker: { type: Number }
})

GymSessionSchema.index({ uuid: 1 }, { name: 'uuidIndex', unique: true })

GymSessionSchema.methods.toJSON = function () {
  var obj = this.toObject()

  delete obj._id
  delete obj.__v

  return obj
}

export default mongoose.model('GymSession', GymSessionSchema)
