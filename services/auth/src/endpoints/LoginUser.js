import express from 'express'
import _ from 'lodash'
import sha1 from 'sha1'
import { v1 as uuid } from 'uuid'
import response from '../helpers/response'

import UserModel from '../models/User'
import TokenModel from '../models/Token'

const LoginUser = express.Router()

LoginUser.post('/loginUser',
  async (req, res, next) => {
    try {
      const searchTerm = { ...req.body }
      searchTerm.password = sha1(searchTerm.password)

      const user = await UserModel.findOne(searchTerm)

      if (!user) {
        return response.error(res, 'AuthorizationFailed', 401)
      }

      const token = await TokenModel.create({
        uuid: uuid(),
        user: user._id
      })

      response.ok(res, token.uuid)
    } catch (e) {
      console.log('LoginUser e', e)
      next(e)
    }
  }
)

export default LoginUser