import express from 'express'
import response from '../helpers/response'

import UserModel from '../models/User'

const DUPLICATE_KEY_ERROR_CODE = 11000

const GetUsers = express.Router()

GetUsers.get('/users',
  async (req, res, next) => {
    try {
      const users = await UserModel.find(req.query)

      response.ok(res, users)
    } catch (e) {
      console.log('GetUsers e', e)
      next(e)
    }
  }
)

export default GetUsers