import express from 'express'
import bodyParser from 'body-parser'

import response from './helpers/response'

import CreateUser from './endpoints/CreateUser'
import GetUser from './endpoints/GetUser'
import LoginUser from './endpoints/LoginUser'
import GetCurrentUser from './endpoints/GetCurrentUser'
import GetUsers from './endpoints/GetUsers'
import UpdateUser from './endpoints/UpdateUser'

const addEndpoints = (app) => {
  app.get('/', (req, res) => { res.send('ok1231') })

  app.use(CreateUser)
  app.use(GetUser)
  app.use(LoginUser)
  app.use(GetCurrentUser)
  app.use(GetUsers)
  app.use(UpdateUser)
}

const app = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json({ limit: '2mb' }))

addEndpoints(app)

app.use('*', function (req, res) {
  response.error(res, 'UnknownPath', 404)
})

app.use((err, req, res, next) => {
  if (res.headersSent) {
    return next()
  }
  console.log('err', err)
  response.error(res, 'InternalError', 500, err)
})

export default app
