import mongoose from 'mongoose'
import { v1 as uuid } from 'uuid'

const UserSchema = new mongoose.Schema({
  uuid: { type: String, required: true, default: uuid },

  email: { type: String, required: true }, // used for login
  password: { type: String, required: true }, // sha1
  role: { type: String, required: true, default: 'user', enum: ['user', 'coach', 'manager', 'admin'] },

  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  photoUrl: { type: String, default: '' },
  phoneNumber: { type: String, required: true },
  gender: { type: String, required: true, enum: ['male', 'female'] },
  signUpDate: { type: Date, required: true, default: Date.now },

  cardNumber: { type: String, default: '' },

  defaultCoachId: { type: String, default: null }
})

UserSchema.index({ uuid: 1 }, { name: 'uuidIndex', unique: true })
UserSchema.index({ email: 1 }, { name: 'emailIndex', unique: true })
UserSchema.index({ phoneNumber: 1 }, { name: 'phoneNumberIndex', unique: true })

UserSchema.methods.toJSON = function () {
  var obj = this.toObject()
  delete obj.password
  delete obj._id
  delete obj.__v
  return obj
}

export default mongoose.model('User', UserSchema)
