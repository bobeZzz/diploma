import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import { apiMiddleware } from 'redux-api-middleware'
import { composeWithDevTools } from 'redux-devtools-extension'
import { reducer as rootReducer, NAME as ROOT_NAME } from './modules/Root/reducer'
import { reducer as usersReducer, NAME as USERS_NAME } from './modules/Users/reducer'
import { reducer as subscriptionReducer, NAME as SUBSCRIPTION_NAME } from './modules/Subscription/reducer'
import { reducer as orderReducer, NAME as ORDER_NAME } from './modules/Order/reducer'
import { reducer as trainingsReducer, NAME as TRAININGS_NAME } from './modules/Trainings/reducer'
import { reducer as adminReducer, NAME as ADMIN_MENU_NAME } from './modules/AdminMenu/reducer'
import { reducer as productReducer, NAME as PRODUCT_NAME } from './modules/Products/reducer'
import { reducer as gymSessionsReducer, NAME as GYM_SESSIONS_NAME } from './modules/GymSessions'
import { reducer as loginReducer, NAME as LOGIN_NAME } from './modules/Login/reducer'
import { reducer as adminDashboardReducer, NAME as ADMIN_DASHBOARD_NAME } from './modules/Dashboard'

const appReducer = combineReducers({
  [ROOT_NAME]: rootReducer,
  [USERS_NAME]: usersReducer,
  [SUBSCRIPTION_NAME]: subscriptionReducer,
  [ORDER_NAME]: orderReducer,
  [TRAININGS_NAME]: trainingsReducer,
  [ADMIN_MENU_NAME]: adminReducer,
  [PRODUCT_NAME]: productReducer,
  [GYM_SESSIONS_NAME]: gymSessionsReducer,
  [ADMIN_DASHBOARD_NAME]: adminDashboardReducer,
  [LOGIN_NAME]: loginReducer
})

const store = createStore(
  appReducer,
  {},
  composeWithDevTools(applyMiddleware(thunk, apiMiddleware))
)

export default store