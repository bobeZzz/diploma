import React from 'react'
function TextInput(props) {
    const { label, placeholder, onChangeHandler, type, isRow, defaultValue } = props
    const wrapClassName = isRow ? 'form-group col-sm-6' : 'form-group'
    if (type === 'checkbox')
        return (
            <div className={wrapClassName+' funkyradio'}>
                <div className='funkyradio-warning'>
                <input
                    id={'checkbox' + label}
                    type={type}
                    onChange={onChangeHandler}
                    defaultChecked={defaultValue !== undefined ?defaultValue:true}
                />
                <label for={'checkbox' + label}>{label}</label>
                </div>
            </div>
        )
    else
        return (
            <div className={wrapClassName}>
                <label>{label}</label>
                <input
                    defaultValue={defaultValue || ''}
                    className='form-control'
                    type={type}
                    placeholder={placeholder}
                    onChange={onChangeHandler}
                />
            </div>
        )
}
export default TextInput