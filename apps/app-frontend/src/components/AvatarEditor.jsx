import React, { Component } from 'react'
import AvatarEditor from 'react-avatar-editor'

class UserAvatarEditor extends Component {
  constructor(props) {
    super()

    this.state = {
      image: props.imageUrl || 'https://res.cloudinary.com/drcjrsq5t/image/upload/v1587587987/Portrait_Placeholder_scbpxj.png',
      scale: 1,
      isEdit: false
    }
  }

  toggleEditMode = () => {
    this.props.toggleHandler(!this.state.isEdit)
    this.setState({ isEdit: !this.state.isEdit })
  }

  render() {
    if (!this.state.isEdit) return (

      <div className="profile-img">
        <div className='profile-container'>
        <img src={this.props.imageUrl} />

        <div className="file btn btn-lg btn-primary btn-file-upload"
          onClick={this.toggleEditMode}>
          Змінити фото
          <input type='button' />          
        </div>
        </div>
      </div>
    )
    return (
      <div className='profile-img'>
        <div>
          <AvatarEditor
            ref={this.props.setEditorRef}
            image={this.state.image}
            scale={this.state.scale}
            width={400}
            height={400}
            style={{ marginBottom: 20 }}
          />
        </div>
        <div className="form-group">
            <div className="custom-file">
              <input type='file'
                  onChange={e => this.setState({ image: e.target.files[0] })}
                  ref={ref => { this.imageInput = ref }} className="custom-file-input" id="inputGroupFile01"
                aria-describedby="inputGroupFileAddon01"/>
                <label className="custom-file-label" for="inputGroupFile01">Choose file</label>
             </div>
            </div>
        <div className='row'>

        <div className='col-sm-6 form-group'>
          <input
            className='col-sm-12'
            type='range'
            min={0.4}
            max={5}
            step={0.1}
            style={{ marginBottom: 15, width: 200 }}
            onChange={(e) => this.setState({ scale: +e.target.value })}
            value={this.state.scale}
          />
        </div>

            <div className='col-sm-6 form-group'>
              <button className='col-sm-6 btn btn-danger' onClick={this.toggleEditMode} >
                Відміна
            </button>
            </div> 
          </div>
      </div>
    )
  }
}

export default UserAvatarEditor