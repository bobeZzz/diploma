import React from 'react'

import { Redirect } from 'react-router'
let isAlertShow = false

export class CustomRedirection extends React.Component {
    
    constructor(props){
        super(props);
      };
    componentDidUpdate (){
        this.props.reset()
    }
    render () {
        return (
            <Redirect to={this.props.redirectTo} /> 
        )
    }
}