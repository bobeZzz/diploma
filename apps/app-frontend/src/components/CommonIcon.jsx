import React from 'react'

export default class CommonIcon extends React.Component {
    render () {
        const {text} = this.props
        return (
        <i className='icon'>{text}</i>
        )
    }
}