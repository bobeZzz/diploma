import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

//import './assets/scss/paper-dashboard.scss?v=1.1.0'
//import 'bootstrap/dist/css/bootstrap.css'
import './assets/css/style.css'
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css'
import 'bootstrap/dist/js/bootstrap.js'

//import 'node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
//import 'bootstrap/dist/js/bootstrap.min.js'


import { Provider } from 'react-redux'


import store from './rootReducer'
ReactDOM.render(
      <Provider store={store}>
        <App />
      </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
