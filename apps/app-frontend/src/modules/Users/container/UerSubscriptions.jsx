import React from 'react';
import { connect, } from 'react-redux'
import { NAME as USERS_NAME } from '../reducer'
import { NAME as ROOT_NAME } from '../../Root/reducer'
import { bindActionCreators } from 'redux'
import { actions as usersActions } from '../actions'

let ReactBsTable = require('react-bootstrap-table');


let BootstrapTable = ReactBsTable.BootstrapTable;
let TableHeaderColumn = ReactBsTable.TableHeaderColumn;

const dateFormatter = (date) => {
  return Intl.DateTimeFormat('uk', {
    year: 'numeric',
    month: 'short',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit'
  }).format(new Date(date))
}

function mapStateToProps(state) {
  return {
    ...state[USERS_NAME],
    currentUser: state[ROOT_NAME].user,
    userRole: state[ROOT_NAME].user.role
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
  ...usersActions
}, dispatch)

class UserSubscriptions extends React.Component {
  constructor(props) {
    super(props)
    let data = {}
    if (['manager', 'admin'].includes(this.props.userRole))
      data = { userId: this.props.selectedUserId }
    props.getUserSubscriptions(data);
  }

  buttonFormatter = (cell, row) => {
    console.log(row)
    if (row.isPaused)
      return (
        <button
          onClick={() => this.props.resumeUserSubscription(row.uuid, row.userId)}
        >
          Продовжити
        </button>
      )
    else
      return (
        <button
          onClick={() => this.props.pauseUserSubscription(row.uuid, row.userId)}
        >
          Призупинити
        </button>
      )
  }

  render() {
    const user = (this.props.uuid ? this.props.user : this.props.currentUser)
    return (
      <div className='emp-content'>
        <h4>Список абонементів</h4>
        <BootstrapTable
          data={this.props.subscriptions}
          striped hover
        >
          <TableHeaderColumn isKey dataField='uuid' hidden>Product ID</TableHeaderColumn>
          <TableHeaderColumn dataField='subscription' dataFormat={(cell, row) => { return cell.name }}>Имя</TableHeaderColumn>
          <TableHeaderColumn dataField='subscription' dataFormat={(cell, row) => { return cell.description }}>Опис</TableHeaderColumn>
          <TableHeaderColumn dataField='endDate' dataFormat={dateFormatter}>Дата закінчення</TableHeaderColumn>
          <TableHeaderColumn dataField='isPaused' dataFormat={value => value ? 'Призупинений' : 'Активний'}>Активний</TableHeaderColumn>
          {['manager', 'admin'].includes(this.props.userRole) &&
            <TableHeaderColumn dataField="button" dataFormat={this.buttonFormatter}>Призупинка/Відновлення</TableHeaderColumn>}
        </BootstrapTable>
      </div>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(UserSubscriptions)
