import React from 'react';
import { connect, } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions as usersActions } from '../actions'
import { NAME as USERS_NAME } from '../reducer'
import { NAME as ROOT_NAME } from '../../Root/reducer'
import { Button } from 'react-bootstrap';
import '../../../assets/font-awesome-4.7.0/css/font-awesome.css'
import AvatarEditor from '../../../components/AvatarEditor'
import Select from 'react-select'
import TextInput from '../../../components/TextInput'

function mapStateToProps(state) {
    return {
        ...state[USERS_NAME],
        userRole: state[ROOT_NAME].user.role
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    ...usersActions
}, dispatch)

class UserRegistration extends React.Component {
    constructor() {
        super()
        this.state = {
            email: '',
            role: 'user',
            firstName: '',
            lastName: '',
            photoUrl: 'https://res.cloudinary.com/drcjrsq5t/image/upload/v1587587987/Portrait_Placeholder_scbpxj.png',
            phoneNumber: '',
            gender: 'male',
            cardNumber: ''
        }
    }
    setEditorRef = (editor) => { this.editor = editor }  //referens to component for image edit
    submitForm = () => {
        let photoBase64
        if (this.editor) {
            photoBase64 = this.editor.getImageScaledToCanvas().toDataURL()
        }
        let data = {            
            email: this.state.email, 
            role: this.state.role, 
            firstName: this.state.firstName, 
            lastName: this.state.lastName, 
            photoBase64, 
            phoneNumber: this.state.phoneNumber, 
            gender: this.state.gender, 
            cardNumber: this.state.cardNumber
        }
        if(this.state.defaultCoachId)
            data.defaultCoachId = this.state.defaultCoachId
        this.props.registerUser(data)
    }
    onImageChange = (value) => {
        this.setState({ isImageEdited: value })
    }
    render() {
        const options = []
        if (['manager', 'admin'].includes(this.props.userRole)) {
            options.push({
                value: 'user',
                label: 'Користувач',
                selected: 'selected'
            })
            options.push({
                value: 'coach',
                label: 'Тренер'
            })
        if(this.props.userRole === 'admin')
        {
            options.push({
                value: 'manager',
                label: 'Менеджер'
            })
            options.push({
                value: 'admin',
                label: 'Адміністратор'
            })
        }
        }
        //----------------------
        const coachOptions = []
        const coaches = this.props.getCoaches()
        coaches.map((coach, key) => {
                coachOptions.push({
                    value: coach.uuid,
                    label: coach.firstName + ' ' + coach.lastName
                })
            })
        //-----------------------------------
        return (
            <div className="container emp-content">
                <h1 className="well">Регистрация пользователя</h1>
                <div className="row">
                    <div className='col well avatar-container'>
                        <AvatarEditor
                            setEditorRef={this.setEditorRef}
                            imageUrl='https://res.cloudinary.com/drcjrsq5t/image/upload/v1587587987/Portrait_Placeholder_scbpxj.png'
                            toggleHandler={this.onImageChange}
                        />
                    </div>
                    <div className="col well">
                            <form>
                                    <div className="row">
                                        <TextInput
                                            isRow
                                            label='Имя'
                                            type="text"
                                            placeholder="Введите имя сюда.."
                                            onChangeHandler={e => this.setState({ firstName: e.target.value })}
                                        />
                                        <TextInput
                                            isRow
                                            label='Фамилия'
                                            type="text"
                                            placeholder="Введите фамилию сюда.."
                                            onChangeHandler={e => this.setState({ lastName: e.target.value })}
                                        />
                                    </div>
                                    <TextInput
                                        label='Карта в спорт зале'
                                        type="text"
                                        placeholder="Введите номер карты в зале.."
                                        onChangeHandler={e => this.setState({ cardNumber: e.target.value })}
                                    />
                                    <div className="row">
                                        <TextInput
                                            isRow
                                            label='Телефон'
                                            type="tel"
                                            placeholder="Введите телефон сюда.."
                                            onChangeHandler={e => this.setState({ phoneNumber: e.target.value })}
                                        />
                                        <TextInput
                                            isRow
                                            label='E-mail'
                                            type="email"
                                            placeholder="Електронный адрес.."
                                            onChangeHandler={e => this.setState({ email: e.target.value })}
                                        />
                                    </div>
                                    <div className='form-group'>
                                    <label>Тренер користувача:</label>
                                    <Select options={coachOptions}
                                        onChange={e => {
                                            this.setState({ defaultCoachId: e.value })
                                        }} />
                                    </div>
                                    {['manager', 'admin'].includes(this.props.userRole)?
                                    <div className='form-group'>
                                    <label>Роль:</label>                                    
                                    <Select options={options}
                                        defaultValue={options[0]}
                                        onChange={e => {
                                            this.setState({ role: e.value })
                                        }} />
                                    </div> 
                                    : '' }            
                                    <div className='form-group'>                                       
                                    <label>Стать:</label>                       
                                    <div className="row">                                    
                                        <div className="col-sm form-group">                                             
                                            <label className="radio inline" >
                                                <input 
                                                    name="gender" type="radio" defaultChecked
                                                    onChange={() => this.setState({ gender: 'male' })} />
                                                <span style={{marginRight: '30px'}}> Чоловік</span>
                                                <label className="radio inline">
                                                <input
                                                    name="gender" type="radio"
                                                    onChange={() => this.setState({ gender: 'female' })} />
                                                <span> Жінка</span>
                                            </label>
                                            </label>
                                        </div>
                                    </div>    
                                    </div>
                            </form>                   
                    </div>
                </div>      
                
                <Button className='form-submit-btn' variant='primary' onClick={() => { this.submitForm() }} >Зареєструвати</Button>          
            </div>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(UserRegistration)
