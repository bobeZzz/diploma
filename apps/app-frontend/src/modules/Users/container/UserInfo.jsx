import React from 'react';
import { connect, } from 'react-redux'
import { NAME as USERS_NAME } from '../reducer'
import { NAME as ROOT_NAME } from '../../Root/reducer'
import { bindActionCreators } from 'redux'
import { actions as usersActions } from '../actions'
import { actions as rootActions } from '../../Root/actions'
 
import InfoRow from '../../../components/InfoRow'


function mapStateToProps(state) {
    return {
        ...state[USERS_NAME],
        currentUser: state[ROOT_NAME].user
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    ...usersActions,
    ...rootActions
}, dispatch)


class UserInfo extends React.Component {
    constructor(props) {
        super(props)
        const { uuid } = props
        if (uuid) {
            if(['manager', 'admin'].includes(this.props.currentUser.role))
            this.props.getUser(this.props.uuid);
        }
        if(['user', 'coach'].includes(this.props.currentUser.role))
        this.props.getCurrentUser();
    }
    componentDidUpdate(prevProps) {
        if (this.props.uuid !== prevProps.uuid) {
            if(['manager', 'admin'].includes(this.props.currentUser.role))
            this.props.getUser(this.props.uuid);
            else
            this.props.getCurrentUser();
        }
    }
    render() {
        const user = (this.props.uuid ? this.props.user : this.props.currentUser)
        return (
            <div className="container emp-content">
                <h2 className="well">Профіль</h2>
                <div className="row">
                    <div className="col avatar-container">
                        <div className="profile-img">
                            <img className='profileImg' src={user.photoUrl} alt="" />
                        </div>
                    </div>
                    <div className="col">
                        <div className="user-info">
                            <InfoRow
                                name="Ім'я"
                                value={user.firstName}
                            />
                            <InfoRow
                                name="Прізвище"
                                value={user.lastName}
                            />
                            <InfoRow
                                name="Телефон"
                                value={user.phoneNumber}
                            />
                            <InfoRow
                                name="Номер картки в залі"
                                value={user.cardNumber}
                            />
                            <InfoRow
                                name="Електронна пошта"
                                value={user.email}
                            />
                            <InfoRow
                                name="Особистий тренер"
                                value={user.coach? user.coach.firstName + ' ' + user.coach.lastName: 'Тренер не призначений'}
                            />
                            <InfoRow
                                name="Стать"
                                value={user.gender === 'male' ? 'Чоловік' : 'Жінка'}
                            />
                            <InfoRow
                                name="Роль"
                                value={
                                    user.role === 'admin'?
                                    'Адміністратор'
                                    :
                                    user.role === 'manager'?
                                    'Менеджер'
                                    :
                                    user.role === 'coach'?
                                    'Тренер'
                                    :
                                    'Клієнт'
                                    }
                            />
                            <InfoRow
                                name="Баланс"
                                value={user.balance}
                            />
                            <InfoRow
                                name="Кількість тренувань"
                                value={user.personalTrainings}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(UserInfo)
