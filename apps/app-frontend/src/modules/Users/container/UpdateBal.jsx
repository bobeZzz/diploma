import React from 'react';
import { connect, } from 'react-redux'
import { bindActionCreators } from 'redux'
import '../../../assets/font-awesome-4.7.0/css/font-awesome.css'
import TextInput from '../../../components/TextInput'
import { NAME as USERS_NAME } from '../reducer'
import { actions as usersActions } from '../actions'
import AvatarEditor from '../../../components/AvatarEditor'
import {Button} from 'react-bootstrap'

function mapStateToProps(state) {
  return {
    currentUser: state[USERS_NAME].user
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
  ...usersActions
}, dispatch)


class UpdateBal extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      amount: 0
    }
  }


  submitForm = () => {
    this.props.updateBal({userId: this.props.currentUser.uuid, amount: this.state.amount})   
  }

  render() {
    const user = this.props.currentUser
    return (
      <div className="container emp-content">
        <h3 className="well">Поповнення балансу</h3>
        <div className='row'>
          <div className="col well">
              <form>
                  <TextInput
                    //defaultValue={user.cardNumber}
                    label='Кiлькість'
                    type="text"
                    placeholder="Введіть суму на яку потрібно поповнити баланс.."
                    onChangeHandler={e => this.setState({ amount: e.target.value })}
                  /> 
              </form>
          </div>
        </div>
        <Button className='form-submit-btn' variant='primary' onClick={() => { this.submitForm() }} >Підтвердити зміни</Button>  
      </div>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(UpdateBal)
