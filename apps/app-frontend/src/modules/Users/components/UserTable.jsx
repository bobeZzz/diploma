import React from 'react'
import qs from 'querystring'
import UserEditButton from './UserEditButton'
import {
  useRouteMatch,
  Link
} from "react-router-dom"
let ReactBsTable = require('react-bootstrap-table');


let BootstrapTable = ReactBsTable.BootstrapTable;
let TableHeaderColumn = ReactBsTable.TableHeaderColumn;

function UserTable(props) {
  function buttonFormatter(cell, row) {
    return <UserEditButton
      setCurrentUser={props.setCurrentUser}
      row={row}
    />
  }
  function roleFormatter(cell, row) {
    return cell === 'admin'?
      'Адміністратор'
      :
      cell === 'manager'?
      'Менеджер'
      :
      cell === 'coach'?
      'Тренер'
      :
      'Клієнт'
  }
  const {
    users,
    displayLinesAmount = 10,
    actionButtons
  } = props
  let { url } = useRouteMatch();
  const roleOptions = {
    admin: 'Адміністратор',
    manager: 'Менеджер',
    coach: 'Тренер',
    user: 'Клієнт'
  }
  return (
    <div className='emp-content'>
      <h4>Список користувачів</h4>
      <BootstrapTable
        data={users}
        options={{
          sizePerPage: displayLinesAmount
        }}
        striped hover
        pagination
        search={true}
        searchPlaceholder='Пошук...'
      >
        <TableHeaderColumn isKey dataField='id' hidden>Product ID</TableHeaderColumn>
        <TableHeaderColumn dataField='firstName'>Имя</TableHeaderColumn>
        <TableHeaderColumn dataField='lastName'>Фамилия</TableHeaderColumn>
        <TableHeaderColumn dataField="cardNumber">Номер карточки</TableHeaderColumn>
        <TableHeaderColumn dataField="phoneNumber">Номер телефона</TableHeaderColumn>
        <TableHeaderColumn filter={ { type: 'SelectFilter', options: roleOptions } } dataFormat={roleFormatter} dataField="role">Роль</TableHeaderColumn>
        <TableHeaderColumn dataField="button" dataFormat={actionButtons || buttonFormatter}>Перейти в профіль</TableHeaderColumn>
      </BootstrapTable>
    </div>
  )
}
export default UserTable