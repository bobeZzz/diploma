export const NAME = 'USERS'
export const types = {
    GET_USERS_REQUEST: `${NAME}/GET_USERS_REQUEST`,
    GET_USERS_SUCCESS: `${NAME}/GET_USERS_SUCCESS`,
    GET_USERS_FAILURE: `${NAME}/GET_USERS_FAILURE`,
    SELECTED_USER_SET: `${NAME}/SELECTED_USER_SET`,
    UPDATE_USER_BY_UUID: `${NAME}/UPDATE_USER_BY_UUID`,
    REQUESTED_USER_INFO_REQUEST: `${NAME}/REQUESTED_UESR_INFO_REQUEST`,
    REQUESTED_USER_INFO_SUCCESS: `${NAME}/REQUESTED_USER_INFO_SUCCESS`,
    REQUESTED_USER_INFO_FAILURE: `${NAME}/REQUESTED_USER_INFO_FAILURE`,
    REQUESTED_USER_SUBSCRIPTIONS_REQUEST: `${NAME}/REQUESTED_UESR_SUBSCRIPTIONS_REQUEST`,
    REQUESTED_USER_SUBSCRIPTIONS_SUCCESS: `${NAME}/REQUESTED_USER_SUBSCRIPTIONS_SUCCESS`,
    REQUESTED_USER_SUBSCRIPTIONS_FAILURE: `${NAME}/REQUESTED_USER_SUBSCRIPTIONS_FAILURE`,
    PAUSE_USER_SUBSCRIPTION_REQUEST: `${NAME}/PAUSE_USER_SUBSCRIPTION_REQUEST`,
    PAUSE_USER_SUBSCRIPTION_SUCCESS: `${NAME}/PAUSE_USER_SUBSCRIPTION_SUCCESS`,
    PAUSE_USER_SUBSCRIPTION_FAILURE: `${NAME}/PAUSE_USER_SUBSCRIPTION_FAILURE`,
    RESUME_USER_SUBSCRIPTION_REQUEST: `${NAME}/RESUME_USER_SUBSCRIPTION_REQUEST`,
    RESUME_USER_SUBSCRIPTION_SUCCESS: `${NAME}/RESUME_USER_SUBSCRIPTION_SUCCESS`,
    RESUME_USER_SUBSCRIPTION_FAILURE: `${NAME}/RESUME_USER_SUBSCRIPTION_FAILURE`,
    REGISTRATION_REQUEST: `${NAME}/REGISTRATION_REQUEST`,
    REGISTRATION_SUCCESS: `${NAME}/REGISTRATION_SUCCESS`,
    REGISTRATION_FAILURE: `${NAME}/REGISTRATION_FAILURE`,
    EDIT_REQUEST: `${NAME}/EDIT_REQUEST`,
    EDIT_SUCCESS: `${NAME}/EDIT_SUCCESS`,
    EDIT_FAILURE: `${NAME}/EDIT_FAILURE`,
    UPDATE_BAL_REQUEST: `${NAME}/UPDATE_BAL_REQUEST`,
    UPDATE_BAL_SUCCESS: `${NAME}/UPDATE_BAL_SUCCESS`,
    UPDATE_BAL_FAILURE: `${NAME}/UPDATE_BAL_FAILURE`
}

export const initialState = {
    selectedUserId: '',
    users: [],
    user: {
        id: ' ',
        role: '',
        photoUrl: 'https://res.cloudinary.com/drcjrsq5t/image/upload/v1587587987/Portrait_Placeholder_scbpxj.png',
        cardNumber: '',
        email: '',
        firstName: '',
        lastName: '',
        phoneNumber: '',
        gender: '',
        password: '',
        uuid: '',
        signUpDate: '',
        __v: 0
    },
    subscriptions: ''
}



export function reducer(state = initialState, action) {
    switch (action.type) {
        case types.GET_USERS_SUCCESS:
            return {
                ...state,
                users: action.payload
            }
        case types.SELECTED_USER_SET:
            return {
                ...state,
                selectedUserId: action.payload
            }
        case types.UPDATE_USER_BY_UUID:
            return {
                ...state,
                users: action.payload
            }
        case types.REQUESTED_USER_INFO_SUCCESS:
            return {
                ...state,
                user: action.payload
            }
        case types.REQUESTED_USER_SUBSCRIPTIONS_SUCCESS:
            return {
                ...state,
                subscriptions: action.payload
            }
        default:
            return { ...state }
    }
}