export const NAME = 'ADMIN_MENU_NAME'
export const types = {
  TEST_VAL_SET: 'TEST_VAL_SET',
  SHOW_ALERT: 'SHOW_ALERT',
  REDIRECT_TO: 'REDIRECT_TO',
  RESET_ALERT: 'RESET_ALERT',
  RESET_REDIRECT: 'RESET_REDIRECT'
}

export const initialState = {
    testn: '24234234',
    alertIsShow: false,
    alert: {
      type: 'primary',
      message: ''
    }
}



export function reducer(state = initialState, action) {
    switch (action.type) {
      case types.TEST_VAL_SET:
        return {
          ...state,
          testParam: action.payload
        }
        case types.REDIRECT_TO:
          return{
            ...state,
            redirect: action.payload
          }
        case types.RESET_REDIRECT:
            return{
              ...state,
              redirect: undefined
            }
        case types.SHOW_ALERT:
        return{
          ...state,
          alert: action.payload,
          alertIsShow: true
        }        
        case types.RESET_ALERT:
        return{
          ...state,
          alertIsShow: false
        }
      default:
        return { ...state }      
    }
  }