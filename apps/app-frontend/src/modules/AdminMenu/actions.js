import {types} from './reducer'

export const actions = {
    setTestValue: (value) => ({type: types.TEST_VAL_SET, payload: value}),
    showAlert: (value) => (
        {
            type: types.SHOW_ALERT,
            payload: value
        }
    ),    
    resetAlert: () => (
        {
            type: types.RESET_ALERT
        }
    ),
    redirectTo: (value) => (
        {
            type: types.REDIRECT_TO, 
            payload: value
        }),
    resetRedirect: () => (
            {
                type: types.RESET_REDIRECT
            })
}