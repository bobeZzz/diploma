import { types, NAME } from './reducer'
import { RSAA } from 'redux-api-middleware'
import Cookies from 'universal-cookie'
import qs from 'querystring'
import {actions as adminActions } from '../AdminMenu/actions'
import {actions as usersActions } from '../Users/actions'
import {actions as productsActions } from '../Products/actions'

const cookies = new Cookies()

const host = process.env.HOST || 'app-gw'

const entryPoint = `http://${host}/api`


export const actions = {
    getSubscriptions: () => ({
        [RSAA]: {
            types: [
                {
                    type: types.GET_SUBSCRIPTIONS_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.GET_SUBSCRIPTIONS_SUCCESS,
                    payload: (action, state, res) => res.json()
                        .then(({data}) => {                            
                            return {
                                data
                            }
                        })

                },
                {
                    type: types.GET_SUBSCRIPTIONS_FAILURE,
                    payload: (action, state, res) => {
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/subscriptions`,
            credentials: 'include',
            method: 'GET'
        }
    }),
    purchaseSubscription: (data,role) => dispatch => dispatch({
        [RSAA]: {
            types: [
                {
                    type: types.PURCHASE_SUBSCRIPTION_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.PURCHASE_SUBSCRIPTION_SUCCESS,
                    payload: (action, state, res) => {
                        if(['manager', 'admin'].includes(role))
                        dispatch(adminActions.redirectTo('/user/list/userinfo/subscriptions'))
                        else                        
                        dispatch(adminActions.redirectTo('/user/subscriptions'))
                            dispatch(adminActions.showAlert({
                                type: 'success',
                                message: 'Абонемент оформлено!'
                            }))                      
                        return
                    }

                },
                {
                    type: types.PURCHASE_SUBSCRIPTION_FAILURE,
                    payload: (action, state, res) => {
                        dispatch(adminActions.showAlert({
                            type: 'danger',
                            message: 'Помилка при спробі оформити абонемент!'
                        }))
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/purchase/subscription`,
            credentials: 'include',
            method: 'POST',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' }
        }
    }),
    purchaseProduct: (data) => dispatch => dispatch({
        [RSAA]: {
            types: [
                {
                    type: types.PURCHASE_PRODUCT_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.PURCHASE_PRODUCT_SUCCESS,
                    payload: (action, state, res) => {    
                        //dispatch(adminActions.redirectTo('/user/list/userinfo/subscriptions'))
                        
                            dispatch(productsActions.getProduct(data.productId))
                            dispatch(usersActions.getUser(data.userId))
                            dispatch(adminActions.showAlert({
                                type: 'success',
                                message: 'Товар куплено!'
                            }))                      
                        return
                    }

                },
                {
                    type: types.PURCHASE_PRODUCT_FAILURE,
                    payload: (action, state, res) => {
                        dispatch(adminActions.showAlert({
                            type: 'danger',
                            message: 'Помилка при купівлі товару!'
                        }))
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/purchase/product`,
            credentials: 'include',
            method: 'POST',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' }
        }
    }),
    purchasePersonalTrainings: (data,role) => dispatch => dispatch({
        [RSAA]: {
            types: [
                {
                    type: types.PURCHASE_TRAINING_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.PURCHASE_TRAINING_SUCCESS,
                    payload: (action, state, res) => {    
                        if(['manager', 'admin'].includes(role))
                        dispatch(adminActions.redirectTo('/user/list/userinfo'))
                        else                        
                        dispatch(adminActions.redirectTo('/user/info'))
                            dispatch(adminActions.showAlert({
                                type: 'success',
                                message: 'Тренування куплено!'
                            }))                      
                        return
                    }

                },
                {
                    type: types.PURCHASE_TRAINING_FAILURE,
                    payload: (action, state, res) => {
                        dispatch(adminActions.showAlert({
                            type: 'danger',
                            message: 'Помилка при спробі купити тренування!'
                        }))
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/purchase/personalTrainings`,
            credentials: 'include',
            method: 'POST',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' }
        }
    }),
    getServices: () => ({
        [RSAA]: {
            types: [
                {
                    type: types.GET_SERVICES_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.GET_SERVICES_SUCCESS,
                    payload: (action, state, res) => res.json()
                        .then(({data}) => {                            
                            return data[0]
                        })

                },
                {
                    type: types.GET_SERVICES_FAILURE,
                    payload: (action, state, res) => {
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/services`,
            credentials: 'include',
            method: 'GET'
        }
    }),
}