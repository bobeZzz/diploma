import React from 'react';
import Cookies from 'universal-cookie'
import { connect, } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Router, Route, Switch, Redirect } from "react-router-dom"
import { createBrowserHistory } from "history"

import AdminMenu from '../../AdminMenu/container/AdminMenu'
import Login from '../../Login/container/Login'
import { actions as rootActions } from '../actions'
import { NAME as ROOT_NAME } from '../reducer'
import { actions as usersActions} from '../../Users/actions'



const hist = createBrowserHistory()
const cookies = new Cookies()

function mapStateToProps(state) {
    return {
        ...state[ROOT_NAME]
    }
}


const mapDispatchToProps = (dispatch) => bindActionCreators({
    ...rootActions,
    ...usersActions
}, dispatch)


class Root extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            token: cookies.get('token') 
        }
        this.Root = React.createRef();
        props.getUsers()
        props.getCurrentUser()
    }
    componentDidUpdate()
    {
        const token = cookies.get('token')
        if(this.state.token !== token)
        {            
            this.props.getUsers()
            this.props.getCurrentUser()
            this.setState({token})
        }
    }
    render() {   
        //this.props.getUser(this.props.user.uuid)     зщсф ту тфвф
        this.props.setCurrentUser(this.props.user.uuid)
        let homePage = '/'
        if(this.props.user.role === 'manager')
        homePage = '/gymsessions'
        else
        if(this.props.user.role === 'user')
        homePage = '/info'
        else
        if(this.props.user.role === 'coach')
        homePage = '/trainings'
        else
        if(this.props.user.role === 'admin')
        homePage = '/dashboard'
        return (
            <Router history={hist}>
                <Switch>
                    <Route
                        path='/login'
                        component={Login}
                    />
                    <Route 
                            path="/"
                            component = {AdminMenu}
                            />                    
                </Switch>
                {this.state.token ?
                        <>
                            <Redirect to={homePage} />
                        </>
                        :
                        <>
                            <Redirect to={'/login'} />
                        </>
                    }
            </Router>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Root)
