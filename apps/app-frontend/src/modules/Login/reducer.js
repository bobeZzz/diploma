export const NAME = 'LOGIN'
export const types = {
  LOGIN_REQUEST: `${NAME}/LOGIN_REQUEST`,
  LOGIN_SUCCESS: `${NAME}/LOGIN_SUCCESS`,
  LOGIN_FAILURE: `${NAME}/LOGIN_FAILURE`
}

export const initialState = {
    ddd: 'fffff',
    loginSuccess: false
}



export function reducer(state = initialState, action) {
    switch (action.type) {
      case types.LOGIN_SUCCESS:
      return {
        ...state,
        loginSuccess: true
      }
      default:
        return { ...state }      
    }
  }