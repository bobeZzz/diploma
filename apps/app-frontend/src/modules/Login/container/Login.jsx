import React from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions as loginActions } from '../actions'
import { NAME as LOGIN_NAME } from '../reducer'
import '../../../assets/css/loginStyle.css'
import '../../../assets/font-awesome-4.7.0/css/font-awesome.css'
import CommonIcon from '../../../components/CommonIcon'
import Cookies from 'universal-cookie'
import { Redirect } from 'react-router';

const cookies = new Cookies()
function mapStateToProps(state) {
    console.log('login: ',state[LOGIN_NAME])
    return {
        ...state[LOGIN_NAME]
    }
}


const mapDispatchToProps = (dispatch) => bindActionCreators({
    ...loginActions
}, dispatch)


class Login extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: ''
        }
    }
    submitForm = () => {
        this.props.logIn(this.state.username, this.state.password)
    }
    render() {
        return (
            <div className="container_login">
                {this.props.loginSuccess && <Redirect to='/'/> }
                
                <div className="d-flex justify-content-center h-100">
                    <div className="card">
                        <div className="card-header">
                            <h3>Вхід в систему</h3>
                        </div>
                        <div className="card-body">
                            <div>
                                <div className="input-group form-group">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text">
                                          <CommonIcon text='rowing'/>    
                                        </span>
                                    </div>
                                    <input 
                                    className="form-control" 
                                    type="text" 
                                    placeholder="username" 
                                    onChange={(e) => { this.setState({ username: e.target.value }) }}
                                    />
                                </div>
                                <div className="input-group form-group">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text">
                                            <CommonIcon text='vpn_key'/>
                                        </span>
                                    </div>
                                    <input 
                                    className="form-control" 
                                    type="password" 
                                    placeholder="password" 
                                    onChange={(e) => { this.setState({ password: e.target.value }) }}
                                    />
                                </div>
                                <div className="form-group">
                                    <button 
                                    type='submit'
                                    className="login_btn btn float-right" 
                                    onClick={() => { this.submitForm() }}                                    
                                    >Війти</button>
                                </div>
                            </div>
                        </div>
                        <div className="card-footer">
                            <div className="d-flex justify-content-center">
                                <a href="#">Забули пароль?</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Login)
