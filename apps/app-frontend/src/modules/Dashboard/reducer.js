export const NAME = 'ADMIN_DASHBOARD'

export const types = {
  GET_REVUE_REQUEST: `${NAME}/GET_REVUE_REQUEST`,
  GET_REVUE_SUCCESS: `${NAME}/GET_REVUE_SUCCESS`,
  GET_REVUE_FAILURE: `${NAME}/GET_REVUE_FAILURE`,

  GET_VISITS_REQUEST: `${NAME}/GET_VISITS_REQUEST`,
  GET_VISITS_SUCCESS: `${NAME}/GET_VISITS_SUCCESS`,
  GET_VISITS_FAILURE: `${NAME}/GET_VISITS_FAILURE`
}

export const initialState = {
  revenue: {},
  visits: {}
}

export function reducer(state = initialState, action) {
  switch (action.type) {
    case types.GET_REVUE_SUCCESS:
      return {
        ...state,
        revenue: action.payload
      }

    case types.GET_VISITS_SUCCESS:
      return {
        ...state,
        visits: action.payload
      }

    default:
      return { ...state }
  }
}