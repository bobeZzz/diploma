import { RSAA } from 'redux-api-middleware'
import qs from 'qs'

import { types, NAME } from './'

const host = process.env.HOST || 'app-gw'

export const entryPoint = `http://${host}/api`

export const actions = {
  getRevenue: (data) => dispatch => dispatch({
    [RSAA]: {
      types: [
        types.GET_REVUE_REQUEST,
        {
          type: types.GET_REVUE_SUCCESS,
          payload: (action, state, res) => {
            return res.json()
              .then(({ data }) => {
                return data
              })
          }
        },
        {
          type: types.GET_REVUE_FAILURE,
          payload: (action, state, res) => {
            return res.json().then(error => {
              return error
            })
          }
        }
      ],
      endpoint: `${entryPoint}/revenue?${qs.stringify(data)}`,
      credentials: 'include',
      method: 'GET'
    }
  }),
  getVisits: (data) => dispatch => dispatch({
    [RSAA]: {
      types: [
        types.GET_VISITS_REQUEST,
        {
          type: types.GET_VISITS_SUCCESS,
          payload: (action, state, res) => {
            return res.json()
              .then(({ data }) => {
                return data
              })
          }
        },
        {
          type: types.GET_VISITS_FAILURE,
          payload: (action, state, res) => {
            return res.json().then(error => {
              return error
            })
          }
        }
      ],
      endpoint: `${entryPoint}/visits?${qs.stringify(data)}`,
      credentials: 'include',
      method: 'GET'
    }
  })
}