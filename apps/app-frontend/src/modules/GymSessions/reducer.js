export const NAME = 'GYM_SESSIONS'

export const types = {
  GET_ACTIVE_SESSIONS_REQUEST: `${NAME}/GET_ACTIVE_SESSIONS_REQUEST`,
  GET_ACTIVE_SESSIONS_SUCCESS: `${NAME}/GET_ACTIVE_SESSIONS_SUCCESS`,
  GET_ACTIVE_SESSIONS_FAILURE: `${NAME}/GET_ACTIVE_SESSIONS_FAILURE`,

  CLOSE_SESSION_REQUEST: `${NAME}/CLOSE_SESSION_REQUEST`,
  CLOSE_SESSION_SUCCESS: `${NAME}/CLOSE_SESSION_SUCCESS`,
  CLOSE_SESSION_FAILURE: `${NAME}/CLOSE_SESSION_FAILURE`,

  REGISTER_USER_SESSION_REQUEST: `${NAME}/REGISTER_USER_SESSION_REQUEST`,
  REGISTER_USER_SESSION_SUCCESS: `${NAME}/REGISTER_USER_SESSION_SUCCESS`,
  REGISTER_USER_SESSION_FAILURE: `${NAME}/REGISTER_USER_SESSION_FAILURE`
}

export const initialState = {
  activeSessions: []
}

export function reducer(state = initialState, action) {
  switch (action.type) {
    case types.GET_ACTIVE_SESSIONS_SUCCESS:
      return {
        ...state,
        activeSessions: action.payload
      }

    case types.CLOSE_SESSION_SUCCESS:
      return {
        ...state,
        activeSessions: action.payload
      }

    case types.REGISTER_USER_SESSION_SUCCESS:
      return {
        ...state,
        activeSessions: action.payload
      }

    default:
      return { ...state }
  }
}