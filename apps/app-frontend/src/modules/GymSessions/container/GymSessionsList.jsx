import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import _ from 'lodash'

import { NAME, actions } from '..'
import { SessionTable } from '../components/SessionTable'
import UserList from '../../Users/container/UserList'
import Modal from '../components/Modal'
import { actions as usersActions } from  '../../Users/actions'
import {
  Route,
  Switch,
  useRouteMatch,
  Link
}
  from "react-router-dom"

function mapStateToProps(state) {
  return {
    ...state[NAME]
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
  ...actions,
  ...usersActions
}, dispatch)


class GymSessionsList extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isShowModal: false,
      locker: null,
      userId: null
    }
  }
  componentDidMount() {
    const { getActiveSessions } = this.props
    getActiveSessions()
  }

  registerUserSession = () => {
    this.props.registerUserSession(this.state.userId, this.state.locker)
    this.setState({
      locker: null,
      userId: null,
      isShowModal: false
    })
  }

  render() {
    const {
      activeSessions,
      closeSession
    } = this.props
    console.log('props',this.props)
    return (
      <>
        <Modal
          show={this.state.isShowModal}
          handleClose={() => this.setState({ locker: null, isShowModal: false })}
          onLockerChange={e => this.setState({ locker: e.target.value })}
          submitAction={this.registerUserSession}
          userName={this.state.userName}
        />
        <UserList
          displayLinesAmount={10}
          actionButtons={(field, data) => {
            return (
              <button
                onClick={() => this.setState({userName: data.firstName, userId: data.uuid, isShowModal: true })}
              >
                Зареєструвати
            </button>
            )
          }}
        />      
      </>
    )
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(GymSessionsList)
