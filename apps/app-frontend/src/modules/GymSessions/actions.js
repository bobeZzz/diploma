import { RSAA } from 'redux-api-middleware'

import { actions as adminActions } from '../AdminMenu/actions'
import { types, NAME } from './reducer'

const host = process.env.HOST || 'app-gw'

const entryPoint = `http://${host}/api`

export const actions = {
  getActiveSessions: () => dispatch => dispatch({
    [RSAA]: {
      types: [
        types.GET_ACTIVE_SESSIONS_REQUEST,
        {
          type: types.GET_ACTIVE_SESSIONS_SUCCESS,
          payload: (action, state, res) => {
            return res.json()
              .then(({ data }) => {
                return data
              })
          }
        },
        {
          type: types.GET_ACTIVE_SESSIONS_FAILURE,
          payload: (action, state, res) => {
            return res.json().then(error => {
              return error
            })
          }
        }
      ],
      endpoint: `${entryPoint}/gymSessions/active`,
      credentials: 'include',
      method: 'GET'
    }
  }),
  closeSession: (uuid) => dispatch => dispatch({
    [RSAA]: {
      types: [
        types.CLOSE_SESSION_REQUEST,
        {
          type: types.CLOSE_SESSION_SUCCESS,
          payload: (action, state, res) => {
            return res.json()
              .then(({ data }) => {
                dispatch(adminActions.showAlert({
                  type: 'success',
                  message: 'Сесія користувача закрита!'
                }))
                const activeSessions = state[NAME].activeSessions.filter(item => item.uuid !== data.uuid)
                return activeSessions
              })
          }
        },
        {
          type: types.CLOSE_SESSION_FAILURE,
          payload: (action, state, res) => {
            return res.json().then(error => {
              dispatch(adminActions.showAlert({
                type: 'danger',
                message: 'Помилка при закритты сесыъ кормстувача!'
              }))
              return error
            })
          }
        }
      ],
      endpoint: `${entryPoint}/gymSessions/close`,
      credentials: 'include',
      method: 'POST',
      body: JSON.stringify({ uuid }),
      headers: { 'Content-Type': 'application/json' }
    }
  }),
  registerUserSession: (userId, locker) => dispatch => dispatch({
    [RSAA]: {
      types: [
        types.CLOSE_SESSION_REQUEST,
        {
          type: types.CLOSE_SESSION_SUCCESS,
          payload: (action, state, res) => {
            return res.json()
              .then(({ data }) => {
                dispatch(adminActions.redirectTo('/gymsessions'))
                dispatch(adminActions.showAlert({
                  type: 'success',
                  message: 'Користувач успішно доданий до сесії!'
                }))
                const activeSessions = [
                  ...state[NAME].activeSessions,
                  data
                ]
                return activeSessions
              })
          }
        },
        {
          type: types.CLOSE_SESSION_FAILURE,
          payload: (action, state, res) => {
            return res.json().then(error => {
              dispatch(adminActions.showAlert({
                type: 'danger',
                message: 'Помилка при додаванні користувача до сесії!'
              }))
              return error
            })
          }
        }
      ],
      endpoint: `${entryPoint}/gymSessions/register`,
      credentials: 'include',
      method: 'POST',
      body: JSON.stringify({ userId, locker }),
      headers: { 'Content-Type': 'application/json' }
    }
  })
}
