import React from 'react'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import TextInput from '../../../components/TextInput'

function ModalWindow(props) {
  const {
    handleClose,
    show,
    onLockerChange,
    submitAction
  } = props
  return (
    <>
      <Modal show={props.show} onHide={props.handleClose}>
        <Modal.Header closeButton>
         <Modal.Title>Введіть номер шафки для {props.userName}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <TextInput
            label='Номер шафки'
            type="nubmer"
            placeholder="Введіть номер шафки сюди.."
            onChangeHandler={onLockerChange}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={props.handleClose}>
            Відміна
         </Button>
          <Button variant="primary" onClick={props.submitAction}>
            Додати до сесії
         </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}
export default ModalWindow