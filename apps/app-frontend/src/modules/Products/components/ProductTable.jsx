import React from 'react'
import qs from 'querystring'
import {
    useRouteMatch,
    Link
} from "react-router-dom"
let ReactBsTable = require('react-bootstrap-table');


let BootstrapTable = ReactBsTable.BootstrapTable;
let TableHeaderColumn = ReactBsTable.TableHeaderColumn;

function ProductTable(props) {
    function imageFormatter(cell, row){
        return <img className="productListImage" src={cell}></img>
    }
    const { row } = props
    let { url } = useRouteMatch();
    return (
        <div className='emp-content'>
            <h4>Список товарів</h4>
        <BootstrapTable
            data={props.products}
            striped hover
            pagination
            search={ true }
            searchPlaceholder='Пошук...'
        >
            <TableHeaderColumn isKey dataField='id' hidden>Product ID</TableHeaderColumn>
            <TableHeaderColumn dataField='photoUrl' dataFormat={imageFormatter}>Фото</TableHeaderColumn>
            <TableHeaderColumn dataField='name'>Назва</TableHeaderColumn>
            <TableHeaderColumn dataField='description'>Опис</TableHeaderColumn>
            <TableHeaderColumn dataField="price">Ціна</TableHeaderColumn>            
            <TableHeaderColumn dataField="amount">Кількість</TableHeaderColumn>
            <TableHeaderColumn dataField="button" dataFormat={props.buttonFormatter}>{props.buttonColumnCaption}</TableHeaderColumn>
        </BootstrapTable>
        </div>
    )
}
export default ProductTable