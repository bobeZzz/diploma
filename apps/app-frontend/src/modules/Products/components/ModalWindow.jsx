import React from 'react'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import { ProductEditLink } from './MatchLink'
function ModalWindow(props) {
   // const [show, setShow] = React.useState(false);

    //const handleClose = () => setShow(false);
    //const handleShow = () => setShow(true);

    return (
        <>
            <Modal show={props.show} onHide={props.handleClose}>
                <Modal.Header closeButton>
                <Modal.Title>Видалити товар?</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                 <p>{props.name}</p>
    <p>{props.description}</p>
                    </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={props.handleClose}>
                        Відміна
                </Button>
                    <Button variant="primary" onClick={props.submitAction}>
                        Видалити
                </Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}
export default ModalWindow