import React from 'react'
import qs from 'querystring'
import ProductTable from './ProductTable'
import ProductInfo from '../container/PtoductInfo'
import EditProduct from '../container/EditProduct'
import ProductOrder from '../../Order/container/ProductOrder'

import {ProductEditButton, ProductByButton} from '../components/ProductButtons'
import {
    Route,
    Switch,
    useRouteMatch,
    Link
}
    from "react-router-dom"
function SwitchPanel(props) {
    const { uuid } = props
    let { path, url } = useRouteMatch();    

    const changeAction = props.changeAction ? props.changeAction: false 
    function buttonFormatter(cell, row){
            if(changeAction === 'By')
            return <ProductByButton
                setCurrentProduct={props.actions.setCurrentProduct}
                getProduct={props.actions.getProduct}
                row={row}/>
            else
            return <ProductEditButton
            setCurrentProduct={props.actions.setCurrentProduct}
            row={row}/>

        }
    return (
        <div className='col'>
            <Switch>
                <Route exact path={path}>
                    <ProductTable
                    buttonColumnCaption={props.buttonColumnCaption}
                    buttonFormatter={buttonFormatter}
                    products={props.products}/>
                </Route>
                <Route exact path={`${path}/productinfo`}>
                    <ProductInfo/>
                </Route>
                <Route exact path={`${path}/productinfo/edit`}>
                    <EditProduct/>
                </Route>
                <Route exact path={`${path}/productinfo/productorder`}>
                    <ProductOrder from='products'/>
                </Route>
            </Switch>
        </div>
    )
}
export default SwitchPanel