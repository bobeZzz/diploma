import { types, NAME } from './reducer'
import { RSAA } from 'redux-api-middleware'
import Cookies from 'universal-cookie'
import {actions as adminActions } from '../AdminMenu/actions'
import qs from 'querystring'

const cookies = new Cookies()

const host = process.env.HOST || 'app-gw'

const entryPoint = `http://${host}/api`


export const actions = {
    getProducts: () => ({
        [RSAA]: {
            types: [
                {
                    type: types.GET_PRODUCTS_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.GET_PRODUCTS_SUCCESS,
                    payload: (action, state, res) => res.json()
                        .then(({data}) => {                            
                            return {
                                products: data,
                            }
                        })

                },
                {
                    type: types.GET_PRODUCTS_FAILURE,
                    payload: (action, state, res) => {
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/products`,
            credentials: 'include',
            method: 'GET'
        }
    }),
    newProduct: (name, description, price, amount, photoBase64) => dispatch => dispatch({
        [RSAA]: {
            types: [
                {
                    type: types.NEW_PRODUCT_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.NEW_PRODUCT_SUCCESS,
                    payload: (action, state, res) => res.json()
                        .then(({ data }) => {       
                            //dispatch(actions.getProducts())                     
                            dispatch(actions.setCurrentProduct(data.uuid))
                            dispatch(adminActions.showAlert({
                                type: 'success',
                                message: 'Товар успішно додано!'
                            }))  
                            //dispatch(console.log(data))    
                            dispatch(adminActions.redirectTo('/products/list/productinfo'))              
                            return
                        })

                },
                {
                    type: types.NEW_PRODUCT_FAILURE,
                    payload: (action, state, res) => {
                        dispatch(adminActions.showAlert({
                            type: 'danger',
                            message: 'Помилка при спробі додати товар!'
                        }))
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/products`,
            credentials: 'include',
            method: 'POST',
            body: JSON.stringify({name, description, price, amount, photoBase64}),
            headers: { 'Content-Type': 'application/json' }
        }
    }),
    setCurrentProduct: (value) => ({
        type: types.SELECTED_PRODUCT_SET, 
        payload: value
    }),
    findProduct: id => (dispatch, getState) => {
        const state = getState()[NAME]
        const product = state.products.find(
            (product) => product.uuid === id)
        dispatch ({
            type: types.FIND_PRODUCT,
            payload: product
        })
    },
    editProduct: (uuid, productData) => dispatch => dispatch({ 
        [RSAA]: {
            types: [
                {
                    type: types.EDIT_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.EDIT_SUCCESS,
                    payload: (action, state, res) => res.json()
                        .then(({data}) => {                            
                            dispatch(adminActions.showAlert({
                                type: 'success',
                                message: 'Товар успішно змінено!'
                            }))  
                            dispatch(adminActions.redirectTo('/products/list/productinfo'))
                            return 
                        })

                },
                {
                    type: types.EDIT_FAILURE,
                    payload: (action, state, res) => {
                        dispatch(adminActions.showAlert({
                            type: 'danger',
                            message: 'Помилка при редагуванні товару!'
                        }))
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/products/${uuid}`,
            credentials: 'include',
            method: 'PATCH',
            body: JSON.stringify(productData),
            headers: { 'Content-Type': 'application/json' }
        }
    }),
    deleteProduct: (uuid) => dispatch => dispatch({
        [RSAA]: {
            types: [
                {
                    type: types.DELETE_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.DELETE_SUCCESS,
                    payload: (action, state, res) => {
                        dispatch(adminActions.showAlert({
                            type: 'success',
                            message: 'Товар успішно видалено!'
                        }))
                        dispatch(actions.getProducts())
                        dispatch(adminActions.redirectTo('/products/list'))    
                        return
                    }
                },
                {
                    type: types.DELETE_FAILURE,
                    payload: (action, state, res) => {
                        return
                    }
                }
            ],
            endpoint: `${entryPoint}/products?${qs.stringify({uuid})}`,
            credentials: 'include',
            method: 'DELETE'
        }
    }),
    getProduct: (uuid) => ({
        [RSAA]: {
            types: [
                {
                    type: types.GET_PRODUCT_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.GET_PRODUCT_SUCCESS,
                    payload: (action, state, res) => res.json()
                        .then(({data}) => {                            
                            return data[0]
                        })
                },
                {
                    type: types.GET_PRODUCT_FAILURE,
                    payload: (action, state, res) => {
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/products?${qs.stringify({uuid})}`,
            credentials: 'include',
            method: 'GET'
        }
    }),

}