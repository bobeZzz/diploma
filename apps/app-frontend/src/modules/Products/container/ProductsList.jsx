import React from 'react';
import { connect, } from 'react-redux'
import { NAME as PRODUCT_NAME } from '../reducer'
import { bindActionCreators } from 'redux'
import { actions as productsActions } from '../actions'
import SwichPanel from '../components/SwichPanel'

let ReactDOM = require('react-dom');

function mapStateToProps(state) {
    return {
        ...state[PRODUCT_NAME]
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    ...productsActions
}, dispatch)

class ProductsList extends React.Component {
    constructor(props) {
        super(props);
        props.getProducts()  
        //props.getProduct('cd2f4410-a513-11ea-962f-99f54b202c42')    
    }
    render() {        
        const { products } = this.props
        return (
            <div className='row'>                    
                    <SwichPanel                        
                        buttonColumnCaption={this.props.changeButtonColumnCaption? this.props.changeButtonColumnCaption : 'Перейти на товар'}
                        changeAction={this.props.changeAction}
                        actions={
                            {setCurrentProduct: this.props.setCurrentProduct,
                            getProduct: this.props.getProduct}
                        }
                        uuid={this.props.selectedProduct} 
                        products={products}/>
            </div>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProductsList)
