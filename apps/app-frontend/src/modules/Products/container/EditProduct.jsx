import React from 'react';
import { connect, } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions as productActions } from '../actions'
import { NAME as PRODUCT_NAME } from '../reducer'
import { Button } from 'react-bootstrap';
import '../../../assets/font-awesome-4.7.0/css/font-awesome.css'
import AvatarEditor from '../../../components/AvatarEditor'

import TextInput from '../../../components/TextInput'

function mapStateToProps(state) {
    return {
        ...state[PRODUCT_NAME]
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    ...productActions
}, dispatch)

class EditProduct extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
                isImageEdited: false
        }
    }
    setEditorRef = (editor) => { this.editor = editor }  //referens to component for image edit
    submitForm = () => {
        let photoBase64
        if (this.editor) {
        photoBase64 = this.editor.getImageScaledToCanvas().toDataURL()
        }

        const data = {
        ...this.state,
        photoBase64
        }

        if (!this.state.isImageEdited) delete data.photoBase64
        delete data.isImageEdited

        this.props.editProduct(this.props.selectedProductId,data)
    }
    onImageChange = (value) => {
        this.setState({ isImageEdited: value })
    }
    render() {
        const product = this.props.product
        return (
            <div className="container emp-content">
                <h1 className="well">Додавання товару</h1>
                <div className="row">
                    <div className='col well avatar-container'>
                        <AvatarEditor
                            setEditorRef={this.setEditorRef}
                            imageUrl={product.photoUrl}
                            toggleHandler={this.onImageChange}
                        />
                    </div>
                    <div className="col well">
                        <div className="row">
                            <form className='col-sm-12'>            
                                    <TextInput
                                        defaultValue={product.name}
                                        label='Назва товару'
                                        type="text"
                                        placeholder="Введіть назву товару.."
                                        onChangeHandler={e => this.setState({ name: e.target.value })}
                                    />   
                                    <TextInput                                    
                                        defaultValue={product.description}
                                        label='Опис товару'
                                        type="text"
                                        placeholder="Введіть опис товару.."
                                        onChangeHandler={e => this.setState({ description: e.target.value })}
                                    />  
                                    <TextInput                                    
                                        defaultValue={product.price}
                                        label='Ціна'
                                        type="number"
                                        placeholder="Введіть ціну за одиницю товару.."
                                        onChangeHandler={e => this.setState({ price: e.target.value })}
                                    />  
                                    <TextInput
                                        defaultValue={product.amount}
                                        label='Кількість'
                                        type="number"
                                        placeholder="Введіть кількість товару на складі.."
                                        onChangeHandler={e => this.setState({ amount: e.target.value })}
                                    />
                            </form>
                        </div>
                    </div>
                </div>                
                <Button className='form-submit-btn' variant='primary' onClick={() => { this.submitForm() }} >Підтвердити зміни</Button> 
            </div>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EditProduct)
