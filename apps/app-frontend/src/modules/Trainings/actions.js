import { types, NAME } from './reducer'
import { RSAA } from 'redux-api-middleware'
import Cookies from 'universal-cookie'
import qs from 'querystring'
import { actions as adminActions } from '../AdminMenu/actions'

const cookies = new Cookies()

const host = process.env.HOST || 'app-gw'

const entryPoint = `http://${host}/api`


export const actions = {
    getTrainings: (data,savePersonal) => ({
        [RSAA]: {
            types: [
                {
                    type: types.REQUESTED_USER_TRAININGS_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.REQUESTED_USER_TRAININGS_SUCCESS,
                    payload: (action, state, res) => res.json()
                        .then(({ data }) => {
                            if(savePersonal)
                            savePersonal(data)
                            return data
                        })
                },
                {
                    type: types.REQUESTED_USER_TRAININGS_FAILURE,
                    payload: (action, state, res) => {
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/trainingEvents?${qs.stringify(data)}`,
            credentials: 'include',
            method: 'GET'
        }
    }),
    createTraining: (requestData) => dispatch => dispatch({
        [RSAA]: {
            types: [
                {
                    type: types.CREATE_TRAINING_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.CREATE_TRAINING_SUCCESS,
                    payload: (action, state, res) => res.json()
                        .then(({ data }) => {
                            //showAlert('success','Тренування успішно додано!')          
                            dispatch(adminActions.showAlert({
                                type: 'success',
                                message: 'Тренування успішно додано!'
                            }))
                            return data
                        })

                },
                {
                    type: types.CREATE_TRAINING_FAILURE,
                    payload: (action, state, res) => {
                        dispatch(adminActions.showAlert({
                            type: 'danger',
                            message: 'Помилка при спробі додати тренування!'
                        }))
                        return 
                    }

                }
            ],
            endpoint: `${entryPoint}/trainingEvents`,
            credentials: 'include',
            method: 'POST',
            body: JSON.stringify(requestData),
            headers: { 'Content-Type': 'application/json' }
        }
    }),
    declineTrainingEvent: (data, refreshTrainings) => dispatch => dispatch({
        [RSAA]: {
            types: [
                {
                    type: types.DECLINE_TRAINING_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.DECLINE_TRAINING_SUCCESS,
                    payload: (action, state, res) => res.json()
                        .then(({ data }) => {
                            refreshTrainings()
                            //showAlert('success','Тренування успішно додано!')          
                            dispatch(adminActions.showAlert({
                                type: 'success',
                                message: 'Тренування успішно відмінено!'
                            }))
                            return data
                        })

                },
                {
                    type: types.DECLINE_TRAINING_FAILURE,
                    payload: (action, state, res) => {
                        dispatch(adminActions.showAlert({
                            type: 'danger',
                            message: 'Помилка при спробі відмінити тренування!'
                        }))
                        return 
                    }

                }
            ],
            endpoint: `${entryPoint}/trainingEvents/decline`,
            credentials: 'include',
            method: 'POST',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' }
        }
    })
}