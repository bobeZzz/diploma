import React from 'react'
import Toolbar from 'react-big-calendar/lib/Toolbar'

export default class CalendarToolbar extends Toolbar {

	componentDidMount() {
		const view = this.props.view
	}

	render() {
		return (
			<div className="rbc-toolbar">
				<div className="rbc-btn-group">
					<button type="button" onClick={() => this.navigate('TODAY')}>сьогодні</button>
					<button type="button" onClick={() => this.navigate('PREV')}>попередній</button>
					<button type="button" onClick={() => this.navigate('NEXT')}>наступний</button>
				</div>
				<div className="rbc-toolbar-label">{this.props.label}</div>
				<div className="rbc-btn-group">
					<button type="button" onClick={this.view.bind(null, 'month')}>місяць</button>
					<button type="button" onClick={this.view.bind(null, 'week')}>тиждень</button>
					<button type="button" onClick={this.view.bind(null, 'day')}>день</button>
					<button type="button" onClick={this.view.bind(null, 'agenda')}>агенда</button>
				</div>
			</div>
		)
	}
}