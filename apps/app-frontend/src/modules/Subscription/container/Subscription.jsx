import React from 'react';
import { connect } from 'react-redux'
import { NAME as SUBSCRIPTION_NAME } from '../reducer'
import { bindActionCreators } from 'redux'
import { actions as subscriptionActions } from '../actions'
import SubscriptionList from '../components/SubscriptionList'
import SubscriptionCreate from '../components/SubscriptionsCreate'
import {
    Route,
    Switch,
    useRouteMatch,
    Link
}
    from "react-router-dom"

function mapStateToProps(state) {
    return {
        ...state[SUBSCRIPTION_NAME]
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
    ...subscriptionActions
}, dispatch)

class Subscription extends React.Component {
    constructor(props) {
        super(props);
        props.getSubscription()
    }
    render() {
        return (
            <Switch>    
                <Route exact path='/subscription/list'>        
                 <SubscriptionList
                 subscriptions = {this.props.subscriptions}
                 deleteSubscription = {this.props.deleteSubscription}
                 />
                 </Route>
                 <Route exact path='/subscription/create'>
                 <SubscriptionCreate
                 createSubscription = {this.props.createSubscription}
                 />
                 </Route>
                 </Switch>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Subscription)
