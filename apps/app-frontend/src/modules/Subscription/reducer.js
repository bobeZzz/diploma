import Subscription from "./container/Subscription"

export const NAME = 'SUBSCRIPTION'
export const types = {
    GET_SUBSCRIPTION_REQUEST: `${NAME}/GET_SUBSCRIPTION_REQUEST`,
    GET_SUBSCRIPTION_SUCCESS: `${NAME}/GET_SUBSCRIPTION_SUCCESS`,
    GET_SUBSCRIPTION_FAILURE: `${NAME}/GET_SUBSCRIPTION_FAILURE`,
    CREATE_SUBSCRIPTION_REQUEST: `${NAME}CREATE_SUBSCRIPTION_REQUEST`,
    CREATE_SUBSCRIPTION_SUCCESS: `${NAME}CREATE_SUBSCRIPTION_SUCCESS`,
    CREATE_SUBSCRIPTION_FAILURE: `${NAME}CREATE_SUBSCRIPTION_FAILURE`,
    DELETE_SUBSCRIPTION_REQUEST: `${NAME}DELETE_SUBSCRIPTION_REQUEST`,
    DELETE_SUBSCRIPTION_SUCCESS: `${NAME}DELETE_SUBSCRIPTION_SUCCESS`,
    DELETE_SUBSCRIPTION_FAILURE: `${NAME}DELETE_SUBSCRIPTION_FAILURE`,
}

export const initialState = {
}



export function reducer(state = initialState, action) {
    switch (action.type) {
        case types.GET_SUBSCRIPTION_SUCCESS:
            return {
                ...state,
                subscriptions: action.payload.data
            }    
        case types.DELETE_SUBSCRIPTION_SUCCESS:
            return{
                ...state
            }        
        default:
            return { ...state }
    }
}