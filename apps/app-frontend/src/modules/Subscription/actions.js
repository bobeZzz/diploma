import { types, NAME } from './reducer'
import { RSAA } from 'redux-api-middleware'
import Cookies from 'universal-cookie'
import qs from 'querystring'
import {actions as adminActions } from '../AdminMenu/actions'

const cookies = new Cookies()

const host = process.env.HOST || 'app-gw'

const entryPoint = `http://${host}/api`


export const actions = {
    getSubscription: () => ({
        [RSAA]: {
            types: [
                {
                    type: types.GET_SUBSCRIPTION_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.GET_SUBSCRIPTION_SUCCESS,
                    payload: (action, state, res) => res.json()
                        .then(({data}) => {                            
                            return {
                                data
                            }
                        })

                },
                {
                    type: types.GET_SUBSCRIPTION_FAILURE,
                    payload: (action, state, res) => {
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/subscriptions`,
            credentials: 'include',
            method: 'GET'
        }
    }),
    createSubscription: (newSubscription) => dispatch => dispatch({
        [RSAA]: {
            types: [
                {
                    type: types.CREATE_SUBSCRIPTION_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.CREATE_SUBSCRIPTION_SUCCESS,
                    payload: (action, state, res) => {
                        dispatch(adminActions.showAlert({
                            type: 'success',
                            message: 'Абонемент створено!'
                        }))  
                        dispatch(adminActions.redirectTo('/subscription/list'))
                        dispatch(actions.getSubscription())
                        return
                    }
                },
                {
                    type: types.CREATE_SUBSCRIPTION_FAILURE,
                    payload: (action, state, res) => {
                        dispatch(adminActions.showAlert({
                            type: 'danger',
                            message: 'Помилка при спробі створити абонемент!'
                        }))  
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/subscriptions`,
            credentials: 'include',
            method: 'POST',
            body: JSON.stringify(newSubscription),
            headers: { 'Content-Type': 'application/json' }
        }
    }),
    editSubscription: (uuid, subscriptionData) => dispatch => dispatch({ 
        [RSAA]: {
            types: [
                {
                    type: types.EDIT_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.EDIT_SUCCESS,
                    payload: (action, state, res) => res.json()
                        .then(({data}) => {                            
                            //dispatch(userListActions.updateUserByUuid(data.user))
                            //dispatch(userListActions.getUsers('user'))
                            return 
                        })

                },
                {
                    type: types.EDIT_FAILURE,
                    payload: (action, state, res) => {
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/subscription/${uuid}`,
            credentials: 'include',
            method: 'PATCH',
            body: JSON.stringify(subscriptionData),
            headers: { 'Content-Type': 'application/json' }
        }
    }),
    deleteSubscription: (uuid) => dispatch => dispatch({
        [RSAA]: {
            types: [
                {
                    type: types.DELETE_SUBSCRIPTION_REQUEST,
                    payload: (action, state, res) => {
                        return
                    }
                },
                {
                    type: types.DELETE_SUBSCRIPTION_SUCCESS,
                    payload: (action, state, res) => {
                        dispatch(actions.getSubscription())
                        dispatch(adminActions.showAlert({
                            type: 'success',
                            message: 'Абонемент видалено!'
                        }))  
                        return
                    }

                },
                {
                    type: types.DELETE_SUBSCRIPTION_FAILURE,
                    payload: (action, state, res) => {
                        dispatch(adminActions.showAlert({
                            type: 'danger',
                            message: 'Помилка при спробі видалити абонемент!'
                        })) 
                        return
                    }

                }
            ],
            endpoint: `${entryPoint}/subscriptions?${qs.stringify({uuid})}`,
            credentials: 'include',
            method: 'DELETE'
        }
    })
}