import app from './app'
import mongoose from 'mongoose'

console.log('app-gw', process.env.NODE_PORT)

app.listen(process.env.NODE_PORT || 80)

console.log('info', 'Started on ', process.env.NODE_PORT || 80)
