import { body, param, query, header, validationResult, oneOf, check } from 'express-validator'
import validatorsCheck from './validatorsCheck'

const validator = {}

validator.createProduct = [
  body('name').isString(),
  body('description').isString(),
  body('price').isFloat({ min: 0.01 }),
  body('amount').isInt({ min: 0 }),
  body('photoBase64').optional(),
  validatorsCheck
]

validator.deleteProduct = [
  query('uuid').isUUID(),
  validatorsCheck
]

validator.updateProduct = [
  param('uuid').isUUID(),
  body('name').optional().isString(),
  body('description').optional().isString(),
  body('price').optional().isFloat({ min: 0.01 }),
  body('amount').optional().isInt({ min: 0 }),
  body('photoBase64').optional(),
  validatorsCheck
]

validator.getProducts = [
  query('uuid').optional().isUUID(),
  validatorsCheck
]

export default validator
