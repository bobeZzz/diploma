import { body, param, query, header, validationResult, oneOf, check } from 'express-validator'
import validatorsCheck from './validatorsCheck'

const validator = {}

validator.getBalance = [
  query('userId').isUUID(),
  validatorsCheck
]

validator.updateBalance = [
  param('userId').isUUID(),
  body('amount').isFloat(),
  validatorsCheck
]

export default validator
