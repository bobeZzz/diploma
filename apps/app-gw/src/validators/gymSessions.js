import { body, param, query, header, validationResult, oneOf, check } from 'express-validator'
import validatorsCheck from './validatorsCheck'

const validator = {}

validator.getActiveGymSessions = [validatorsCheck]

validator.registerGymSession = [
  body('userId').isUUID(),
  body('locker').isInt(),
  validatorsCheck
]

validator.closeGymSession = [
  body('uuid').isUUID(),
  validatorsCheck
]

export default validator
