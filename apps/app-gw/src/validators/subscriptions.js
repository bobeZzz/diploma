import { body, param, query, header, validationResult, oneOf, check } from 'express-validator'
import validatorsCheck from './validatorsCheck'

const validator = {}

validator.getUserSubscriptions = [
  oneOf([
    [
      header('_role').isIn(['user', 'coach']),
      query('userId').not().exists().customSanitizer((value, { req }) => req.user.id)
    ],
    [
      header('_role').isIn(['manager', 'admin']),
      query('userId').isUUID()
    ]
  ], ('roleAccessDenied')),
  validatorsCheck
]

validator.createSubscription = [
  body('name').isString(),
  body('description').isString(),
  body('price').isFloat({ min: 0.01 }),
  validatorsCheck
]

validator.updateSubscription = [
  param('uuid').isUUID(),
  body('name').optional().isString(),
  body('description').optional().isString(),
  body('price').optional().isFloat({ min: 0.01 }),
  validatorsCheck
]

validator.getSubscriptions = [
  query('uuid').optional().isUUID(),
  query('name').optional().isString(),
  validatorsCheck
]

validator.deleteSubscriptions = [
  query('uuid').isUUID(),
  validatorsCheck
]

validator.pauseUserSubscription = [
  param('uuid').isUUID(),
  validatorsCheck
]

validator.resumeUserSubscription = [
  param('uuid').isUUID(),
  validatorsCheck
]

export default validator
