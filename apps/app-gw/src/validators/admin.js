import { body, param, query, header, validationResult, oneOf, check, cookie } from 'express-validator'

import validatorsCheck from './validatorsCheck'

const validator = {}

validator.getRevenue = [
  query('date[from]').isISO8601(),
  query('date[to]').isISO8601(),
  validatorsCheck
]

validator.getVisists = [
  query('date[from]').isISO8601(),
  query('date[to]').isISO8601(),
  validatorsCheck
]

validator.getRevenueReport = [
  query('date[from]').isISO8601(),
  query('date[to]').isISO8601(),
  validatorsCheck
]

export default validator
