
import { validationResult } from 'express-validator'

import response from '../helpers/response'

const validatorsCheck = (req, res, next) => {
  const errors = validationResult(req)

  if (!errors.isEmpty()) {
    return response.error(res, 'ValidationError', 422, { errors: errors.array() })
  }

  next()
}

export default validatorsCheck