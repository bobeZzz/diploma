import { body, param, query, header, validationResult, oneOf, check, cookie } from 'express-validator'
import sha1 from 'sha1'

import authService from '../services/auth'
import validatorsCheck from './validatorsCheck'

const validator = {}

validator.createUser = [
  body('email').isEmail(),
  body('firstName').isString(),
  body('lastName').isString(),
  body('photoBase64').optional(),
  body('phoneNumber').isMobilePhone('uk-UA'),
  body('gender').isIn(['male', 'female']),
  body('cardNumber').isString().isLength({ min: 13, max: 13 }),
  body('defaultCoachId').optional().isUUID(),
  oneOf([
    [
      header('_role').equals('admin'),
      body('role').isIn(['user', 'coach', 'manager', 'admin'])
    ],
    [
      header('_role').equals('manager'),
      body('role').isIn(['user', 'coach'])
    ]
  ]),
  validatorsCheck
]

validator.updateUser = [
  param('uuid').isUUID(),
  body('email').optional().isEmail(),
  body('firstName').optional().isString(),
  body('lastName').optional().isString(),
  body('photoBase64').optional(),
  body('phoneNumber').optional().isMobilePhone('uk-UA'),
  body('gender').optional().isIn(['male', 'female']),
  body('cardNumber').optional().isString().isLength({ min: 13, max: 13 }),
  body('defaultCoachId').optional().isUUID(),
  oneOf([
    [
      header('_role').equals('admin'),
      body('role').optional().isIn(['user', 'coach', 'manager', 'admin'])
    ],
    [
      header('_role').equals('manager'),
      body('role').optional().isIn(['user', 'coach'])
    ]
  ]),
  validatorsCheck
]

validator.login = [
  oneOf([
    body('email').isEmail(),
    body('phoneNumber').isMobilePhone('uk-UA')
  ]),
  body('password').isString(),
  validatorsCheck
]

validator.getCurrentUser = [
  cookie('token').isUUID(),
  validatorsCheck
]

validator.getUser = [
  oneOf([
    [
      header('_role').equals('user'),
      query('uuid').optional().custom((value, { req }) => req.user.id === value)
    ],
    [
      header('_role').isIn(['manager', 'admin']),
      query('uuid').isUUID()
    ]
  ]),
  validatorsCheck
]

validator.getUsers = [
  oneOf([
    [
      header('_role').isIn(['manager', 'admin']),
      query('role').optional().isIn(['user', 'coach', 'manager', 'admin'])
    ],
    [
      header('_role').equals('coach'),
      query('defaultCoachId').not().exists().customSanitizer((value, { req }) => req.user.id)
    ]
  ]),
  validatorsCheck
]

validator.updateOwnProfile = [
  body('userId').not().exists().customSanitizer((value, { req }) => req.user.id),
  body('defaultCoachId')
    .optional()
    .custom((value, { req }) => req.user.role === 'user')
    .customSanitizer((value) => value ? value : null),
  oneOf([
    [
      body('oldPassword').custom(async (value, { req }) => {
        const user = await authService.getCurrentUser({ token: req.cookies.token })
        return sha1(value) === user.password
          ? Promise.resolve()
          : Promise.reject()
      }),
      body('newPassword').isString()
    ],
    [
      body('oldPassword').not().exists(),
      body('newPassword').not().exists()
    ]
  ]),
  validatorsCheck
]

export default validator
