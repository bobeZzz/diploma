import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import cookieParser from 'cookie-parser'
import path from 'path'

import response from './helpers/response'
import auth from './routes/auth'
import balance from './routes/balance'
import subscription from './routes/subscription'
import orders from './routes/orders'
import trainingEvents from './routes/trainingEvents'
import products from './routes/products'
import gymSessions from './routes/gymSessions'
import services from './routes/services'
import admin from './routes/admin'
import userAuthorizationMiddleware from './helpers/userAuthorizationMiddleware'
import { login } from './controllers/auth'

const addEndpoints = (app) => {
  app.use(express.static(path.join(__dirname, '../build')))
  app.get('/', (req, res) => {
    console.log('path', path.join(__dirname, '../build', 'index.html'))
    res.sendFile(path.join(__dirname, '../build', 'index.html'))
  })

  // all
  app.post('/api/login', login)

  app.use(userAuthorizationMiddleware)
  app.use('/api', auth)
  app.use('/api', balance)
  app.use('/api', subscription)
  app.use('/api', orders)
  app.use('/api', trainingEvents)
  app.use('/api', products)
  app.use('/api', gymSessions)
  app.use('/api', services)
  app.use('/api', admin)
}

const app = express()

app.use(cookieParser())
app.use(
  cors({
    credentials: true,
    origin: true
  })
)

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json({ limit: '2mb' }))

addEndpoints(app)

app.use('*', function (req, res) {
  console.log('req.url', req.url)
  response.error(res, 'UnknownPath', 404)
})

app.use((err, req, res, next) => {
  console.log('req.url', req.url)
  if (res.headersSent) {
    return next()
  }
  console.log('gw err', err)
  response.error(res, 'InternalError', 500, err)
})

export default app
