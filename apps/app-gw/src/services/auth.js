import axios from 'axios'
import qs from 'qs'

class AuthService {
  constructor() {

    this._requestTimeout = 10000
    this._host = 'http://service-auth'
  }

  async _patch(endpoint, uuid, body = {}) {
    try {
      const response = await axios.patch(this._host + endpoint + '/' + uuid, body, { timeout: this._requestTimeout })
      return response.data.data
    } catch (e) {
      // should be present in all errors from service
      throw { type: e.response.data.data.type }
    }
  }

  async _post(endpoint, body = {}) {
    try {
      const response = await axios.post(this._host + endpoint, body, { timeout: this._requestTimeout })
      return response.data.data
    } catch (e) {
      // should be present in all errors from service
      throw { type: e.response.data.data.type }
    }
  }

  async _get(endpoint, query = {}) {
    const queryString = qs.stringify(query)
    try {
      const response = await axios.get(this._host + endpoint + '?' + queryString, { timeout: this._requestTimeout })
      return response.data.data
    } catch (e) {
      // should be present in all errors from service
      throw { type: e.response.data.data.type }
    }
  }

  async testEndpointCall() {
    return axios.get(this._host + '/', { timeout: this._requestTimeout })
  }

  async createUser(body) {
    return this._post('/user', body)
  }

  async getUser(query) {
    return this._get('/user', query)
  }

  async loginUser(body) {
    return this._post('/loginUser', body)
  }

  async getCurrentUser(query) {
    return this._get('/currentUser', query)
  }

  async getUsers(query) {
    return this._get('/users', query)
  }

  async updateUser(uuid, body) {
    return this._patch('/user', uuid, body)
  }
}

export default new AuthService()