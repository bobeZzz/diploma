import axios from 'axios'
import qs from 'qs'

class SalesService {
  constructor() {

    this._requestTimeout = 10000
    this._host = 'http://service-sales'
  }

  async _patch(endpoint, uuid, body = {}) {
    try {
      const response = await axios.patch(this._host + endpoint + '/' + uuid, body, { timeout: this._requestTimeout })
      return response.data.data
    } catch (e) {
      // should be present in all errors from service
      throw { type: e.response.data.data.type }
    }
  }

  async _post(endpoint, body = {}) {
    try {
      const response = await axios.post(this._host + endpoint, body, { timeout: this._requestTimeout })
      return response.data.data
    } catch (e) {
      // should be present in all errors from service
      throw { type: e.response.data.data.type }
    }
  }

  async _get(endpoint, query = {}) {
    const queryString = qs.stringify(query)
    try {
      const response = await axios.get(this._host + endpoint + '?' + queryString, { timeout: this._requestTimeout })
      return response.data.data
    } catch (e) {
      // should be present in all errors from service
      throw { type: e.response.data.data.type }
    }
  }

  async _delete(endpoint, query = {}) {
    const queryString = qs.stringify(query)
    try {
      const response = await axios.delete(this._host + endpoint + '?' + queryString, { timeout: this._requestTimeout })
      return response.data.data
    } catch (e) {
      // should be present in all errors from service
      throw { type: e.response.data.data.type }
    }
  }

  async testEndpointCall() {
    return axios.get(this._host + '/', { timeout: this._requestTimeout })
  }

  async updateBalance(userId, body) {
    return this._patch('/balance', userId, body)
  }

  async getBalance(userId) {
    return this._get('/balance', { userId })
  }

  async createSubscription(body) {
    return this._post('/subscriptions', body)
  }

  async getSubscriptions(query) {
    return this._get('/subscriptions', query)
  }

  async deleteSubscription(uuid) {
    return this._delete('/subscriptions', { uuid })
  }

  async updateSubscription(uuid, body) {
    return this._patch('/subscriptions', uuid, body)
  }

  async purchase(body) {
    return this._post('/purchase', body)
  }

  async createProduct(body) {
    return this._post('/products', body)
  }

  async getProducts(query) {
    return this._get('/products', query)
  }

  async deleteProduct(uuid) {
    return this._delete('/products', { uuid })
  }

  async updateProduct(uuid, body) {
    return this._patch('/products', uuid, body)
  }

  async getOrders(query) {
    return this._get('/orders', query)
  }

  async createService(body) {
    return this._post('/services', body)
  }

  async getServices(query) {
    return this._get('/services', query)
  }

  async deleteService(uuid) {
    return this._delete('/services', { uuid })
  }

  async updateService(uuid, body) {
    return this._patch('/services', uuid, body)
  }
}

export default new SalesService()