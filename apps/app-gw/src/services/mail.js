import nodemailer from 'nodemailer'

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: process.env.MAIL_USER,
    pass: process.env.MAIL_PASSWORD
  }
})

export const sendMail = async (targetEmail, subject, content) => {
  await transporter.sendMail({ to: targetEmail, subject, html: content })
}
