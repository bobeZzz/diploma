import _ from 'lodash'
import cloudinary from 'cloudinary'

import response from '../helpers/response'

import salesService from '../services/sales'


export const createProduct = async (req, res, next) => {
  try {
    const { photoBase64 } = req.body
    const allowedProps = ['name', 'description', 'price', 'amount'] //photoBase64

    const productData = _.pick(req.body, allowedProps)

    if (photoBase64) {
      const { url } = await cloudinary.v2.uploader.upload(req.body.photoBase64)
      productData.photoUrl = url
    } else {
      productData.photoUrl = 'https://res.cloudinary.com/drcjrsq5t/image/upload/v1591086428/img-default_gzuwoy.gif'
    }

    const product = await salesService.createProduct(productData)

    response.ok(res, product, 201)
  } catch (e) {
    console.log('createProduct error', e)
    next(e)
  }
}

export const updateProduct = async (req, res, next) => {
  try {
    const { uuid } = req.params
    const { photoBase64 } = req.body
    const allowedProps = ['name', 'description', 'price', 'amount'] //photoBase64

    const productData = _.pick(req.body, allowedProps)

    if (photoBase64) {
      const { url } = await cloudinary.v2.uploader.upload(req.body.photoBase64)
      //console.log(url)
      productData.photoUrl = url
    }

    const product = await salesService.updateProduct(uuid, productData)

    response.ok(res, product, 201)
  } catch (e) {
    console.log('updateProduct error', e)
    next(e)
  }
}

export const getProducts = async (req, res, next) => {
  try {
    const allowedProps = ['uuid']
    const products = await salesService.getProducts(_.pick(req.query, allowedProps))

    response.ok(res, products, 200)
  } catch (e) {
    console.log('getProducts error', e)
    next(e)
  }
}

export const deleteProduct = async (req, res, next) => {
  try {
    const product = await salesService.deleteProduct(req.query.uuid)

    response.ok(res, product)
  } catch (e) {
    console.log('deleteSubscription error', e)
    next(e)
  }
}