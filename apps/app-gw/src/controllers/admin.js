import _ from 'lodash'
import moment from 'moment'
const csv = require('fast-csv')

import response from '../helpers/response'
import workoutService from '../services/workout'
import salesService from '../services/sales'

import { matchedData } from 'express-validator'

export const getRevenue = async (req, res, next) => {
  try {
    const validData = matchedData(req)

    const orders = await salesService.getOrders({
      date: { $gte: validData.date.from, $lte: validData.date.to }
    })

    const groupedByDays = _.groupBy(orders, value => moment(value.date).startOf('day').format())

    const result = {}

    const startDate = moment(validData.date.from)
    const endDate = moment(validData.date.to)

    let tempKey
    do {
      tempKey = startDate.clone().startOf('day').format()
      result[tempKey] = 0
    } while (startDate.add(1, 'days').diff(endDate) < 0)

    Object.keys(groupedByDays).map(key => {
      groupedByDays[key].map(order => {
        result[key] = +(result[key] + order.amount * order.product.price)
      })
    })

    response.ok(res, result)
  } catch (e) {
    console.log('getRevenue e', e)
    next(e)
  }
}

export const getVisists = async (req, res, next) => {
  try {
    const validData = matchedData(req)

    const orders = await workoutService.getGymSessions({
      endDate: { $gte: validData.date.from, $lte: validData.date.to }
    })

    const groupedByDays = _.groupBy(orders, value => moment(value.endDate).startOf('day').format())

    const result = {}

    const startDate = moment(validData.date.from)
    const endDate = moment(validData.date.to)

    let tempKey
    do {
      tempKey = startDate.clone().startOf('day').format()
      result[tempKey] = 0
    } while (startDate.add(1, 'days').diff(endDate) < 0)

    Object.keys(groupedByDays).map(key => {
      groupedByDays[key].map(order => {
        result[key]++
      })
    })

    response.ok(res, result)
  } catch (e) {
    console.log('getVisists e', e)
    next(e)
  }
}

export const getRevenueReport = async (req, res, next) => {
  try {
    const validData = matchedData(req)

    const orders = await salesService.getOrders({
      date: { $gte: validData.date.from, $lte: validData.date.to }
    })

    const preparedData = _.groupBy(orders, item => item.product.name + '/' + item.product.price)

    let result = {}
    Object.keys(preparedData).map(key => {
      const [name, price] = key.split('/')
      result[key] = {
        name,
        price: +price,
        amount: 0,
        revenue: 0
      }

      preparedData[key].map(order => {
        result[key].amount = result[key].amount + order.amount
        result[key].revenue = +(result[key].revenue + order.amount * order.product.price).toFixed(2)
      })
    })

    result = Object.keys(result).map(key => result[key])
    const total = {
      name: 'Total:',
      price: '',
      amount: _.reduce(result, (result, value) => result + value.amount, 0),
      revenue: _.reduce(result, (result, value) => result + value.revenue, 0)
    }

    res.setHeader('Content-disposition', 'attachment; filename=revenueReport.csv');
    res.setHeader('content-type', 'text/csv');
    var csvStream = csv.format({ objectMode: true })
    csvStream.pipe(res)
    csvStream.write(['name', 'price', 'amount', 'revenue'])
    result.map(item => csvStream.write(item))
    csvStream.write(total)

    csvStream.end()
  } catch (e) {
    console.log('getRevenueReport e', e)
    next(e)
  }
}