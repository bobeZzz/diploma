import _ from 'lodash'
import moment from 'moment'

import response from '../helpers/response'

import authService from '../services/auth'
import salesService from '../services/sales'
import workoutService from '../services/workout'

import { isAdminOrManager } from '../helpers/common'
import { matchedData } from 'express-validator'



export const purchaseSubscription = async (req, res, next) => {
  try {
    const { productId, startDate = Date.now(), amount = 1 } = req.body

    let userId, isCash
    if (isAdminOrManager(req.user.role)) {
      isCash = req.body.isCash
      userId = req.body.userId
    } else {
      isCash = false
      userId = req.user.id
    }

    let user
    if (userId) {
      user = await authService.getUser({ uuid: userId })
      if (!user) return response.error(res, 'UserNotFound', 400)
    }

    const [subscription] = await salesService.getSubscriptions({ uuid: productId })
    if (!subscription) return response.error(res, 'SubscrptionNotFound', 400)

    let balance
    if (!isCash) {
      balance = await salesService.getBalance(userId)

      if (balance.balance < subscription.price * amount) return response.error(res, 'LackOfFunds', 409)

      balance = await salesService.updateBalance(userId, { amount: -Math.abs(subscription.price * amount) })
    }

    const orderData = {
      productType: 'subscription',
      product: subscription,
      userId,
      amount
    }

    const order = await salesService.purchase(orderData)

    const userSubscriptionData = {
      userId,
      subscriptionId: subscription.uuid,
      startDate: moment(startDate).startOf('day'),
      endDate: moment(startDate).endOf('day').add(orderData.amount * 30, 'day')
    }

    const userSubscription = await workoutService.addUserSubscription(userSubscriptionData)

    response.ok(res, { order, balance, userSubscription }, 201)
  } catch (e) {
    console.log('purchaseSubscription e', e)
    next(e)
  }
}

export const purchaseProduct = async (req, res, next) => {
  try {
    const { productId, userId, isCash, amount = 1 } = req.body
    // disable is cash for purchase done by user
    let user
    if (userId) {
      user = await authService.getUser({ uuid: userId })
      if (!user) return response.error(res, 'UserNotFound', 400)
    }

    const [product] = await salesService.getProducts({ uuid: productId })
    if (!product) return response.error(res, 'ProductNotFound', 400)
    if (product.amount < amount) return response.error(res, 'ProductLackAmount', 409)

    let balance
    if (!isCash) {
      balance = await salesService.getBalance(userId)

      if (balance.balance < product.price * amount) return response.error(res, 'LackOfFunds', 409)

      balance = await salesService.updateBalance(userId, { amount: -Math.abs(product.price * amount) })
    }
    await salesService.updateProduct(product.uuid, { amount: product.amount - amount })

    const allowedProps = ['userId', 'amount']

    const orderData = _.pick(req.body, allowedProps)
    orderData.productType = 'product'
    orderData.product = product

    const order = await salesService.purchase(orderData)

    response.ok(res, { order, balance, product }, 201)
  } catch (e) {
    console.log('purchaseProduct error', e)
  }
}

export const purchasePersonalTrainings = async (req, res, next) => {
  try {
    const { userId, amount, isCash } = matchedData(req, { includeOptionals: true })

    const serviceType = 'personalTraining'
    const service = (await salesService.getServices({ type: serviceType }))[0]

    if (!service) return response.error(res, 'FeatureDisabled', 404)

    let user = await authService.getUser({ uuid: userId })
    if (!user) return response.error(res, 'UserNotFound', 400)

    let balance
    if (!isCash) {
      balance = await salesService.getBalance(userId)

      if (balance.balance < service.price * amount) return response.error(res, 'LackOfFunds', 409)

      balance = await salesService.updateBalance(userId, { amount: -Math.abs(service.price * amount) })
    }

    const order = await salesService.purchase({
      userId,
      amount,
      product: service,
      productType: 'service'
    })

    balance = await salesService.updateBalance(userId, { personalTrainings: amount })

    response.ok(res, { user, balance, order })
  } catch (e) {
    console.log('purchasePersonalTrainings e', e)
  }

}