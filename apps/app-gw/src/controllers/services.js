import _ from 'lodash'
import { matchedData } from 'express-validator'

import response from '../helpers/response'

import workoutService from '../services/workout'
import salesService from '../services/sales'

export const createService = async (req, res, next) => {
  try {
    // name price
    const validData = matchedData(req, { locations: 'body' })
    const service = await salesService.createService(validData)

    response.ok(res, service)
  } catch (e) {
    console.log('createService e', e)
    next(e)
  }
}

export const updateService = async (req, res, next) => {
  try {
    // name price
    const validData = matchedData(req, { locations: 'body' })
    const service = await salesService.updateService(req.params.uuid, validData)

    response.ok(res, service)
  } catch (e) {
    console.log('updateService e', e)
    next(e)
  }
}

export const getServices = async (req, res, next) => {
  try {
    // uuid
    const validData = matchedData(req, { locations: 'query' })
    const services = await salesService.getServices(validData)

    response.ok(res, services)
  } catch (e) {
    console.log('getServices e', e)
    next(e)
  }
}

export const deleteService = async (req, res, next) => {
  try {
    // uuid
    const validData = matchedData(req, { locations: 'params' })
    const service = await salesService.deleteService(validData)

    response.ok(res, service)
  } catch (e) {
    console.log('deleteService e', e)
    next(e)
  }
}

export const updatePersonalTrainingService = async (req, res, next) => {
  try {
    const validData = matchedData(req, { includeOptionals: true })
    let service = (await salesService.getServices({ type: 'personalTraining' }))[0]

    service = await salesService.updateService(service.uuid, validData)

    response.ok(res, service)
  } catch (e) {
    console.log('updatePersonalTrainingService e', e)
    next(e)
  }
}