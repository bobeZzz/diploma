import passwordGenerator from 'generate-password'
import _ from 'lodash'
import cloudinary from 'cloudinary'

import authService from '../services/auth'
import salesService from '../services/sales'
import response from '../helpers/response'
import { sendMail } from '../services/mail'
import { matchedData } from 'express-validator'

export const createUser = async (req, res, next) => {
  try {
    const { photoBase64 } = req.body
    const userFields = [
      'email', 'role', 'firstName',
      'lastName', 'photoBase64', 'phoneNumber',
      'gender', 'cardNumber', 'defaultCoachId'
    ]
    const userData = _.pick(req.body, userFields)
    userData.password = passwordGenerator.generate({
      length: 6,
      numbers: true
    })

    sendMail(
      userData.email,
      'Пароль для входу до системи bodyfit',
      `Ваш пароль для входу до системи: ${userData.password}`
    )

    if (photoBase64) {
      const { url } = await cloudinary.v2.uploader.upload(req.body.photoBase64)
      userData.photoUrl = url
    } else {
      userData.photoUrl = 'https://res.cloudinary.com/drcjrsq5t/image/upload/v1587587987/Portrait_Placeholder_scbpxj.png'
    }

    console.log('userData', userData)
    const user = await authService.createUser(userData)

    response.ok(res, { user }, 201)
  } catch (e) {
    console.log('createUser error', e)
    next(e)
  }
}


export const login = async (req, res, next) => {
  try {
    const searchOptions = ['email', 'phoneNumber', 'password']
    const searchTerm = _.pick(req.body, searchOptions)

    const token = await authService.loginUser(searchTerm)
    res.cookie('token', token)
    response.ok(res, { token })
  } catch (e) {
    console.log('login error', e)
    response.error(res, e.type, 401, e)
  }
}

export const getCurrentUser = async (req, res, next) => {
  try {
    const user = await authService.getCurrentUser({ token: req.cookies.token })
    user.coach = await authService.getUser({ uuid: user.defaultCoachId })

    const balance = await salesService.getBalance(user.uuid)
    user.balance = balance.balance
    user.personalTrainings = balance.personalTrainings

    response.ok(res, { user })
  } catch (e) {
    console.log('getCurrentUser error', e)
    next(e)
  }
}

export const updateUser = async (req, res, next) => {
  try {
    const { photoBase64 } = req.body
    const editableProperties = [
      'email', 'password', 'role', 'firstName',
      'lastName', 'photoBase64', 'phoneNumber',
      'gender', 'cardNumber', 'defaultCoachId'
    ]
    const newProperties = _.pick(req.body, editableProperties)

    if (photoBase64) {
      const { url } = await cloudinary.v2.uploader.upload(req.body.photoBase64)
      newProperties.photoUrl = url
    }

    const user = await authService.updateUser(req.params.uuid, newProperties)

    response.ok(res, { user })
  } catch (e) {
    console.log('updateUser error', e)
    next(e)
  }
}

export const getUser = async (req, res, next) => {
  try {
    const queryOptions = ['uuid']
    const searchTerm = _.pick(req.query, queryOptions)

    const user = await authService.getUser(searchTerm)
    user.coach = await authService.getUser({ uuid: user.defaultCoachId })

    const balance = await salesService.getBalance(user.uuid)
    user.balance = balance.balance
    user.personalTrainings = balance.personalTrainings

    response.ok(res, { user })
  } catch (e) {
    console.log('getUser error', e)
    next(e)
  }
}

export const getUsers = async (req, res, next) => {
  try {
    const users = await authService.getUsers(req.query)

    response.ok(res, { users })
  } catch (e) {
    console.log('getUsers error', e)
    next(e)
  }
}

export const updateOwnProfile = async (req, res, next) => {
  try {
    const validData = matchedData(req)

    const requestData = { password: validData.newPassword }
    if (validData.defaultCoachId) requestData.defaultCoachId = validData.defaultCoachId

    const user = await authService.updateUser(validData.userId, requestData)

    response.ok(res, user)
  } catch (e) {
    console.log('updateOwnProfile error', e)
    next(e)
  }
}