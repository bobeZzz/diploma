import response from './response'

const isAdmin = (userRole) => userRole === 'admin'

const roleAccessMiddleware = (allowedRoles) => {
  return (req, res, next) => {
    if (typeof allowedRoles === 'string') allowedRoles = [allowedRoles]

    if (isAdmin(req.user.role)) return next()

    if (allowedRoles.includes(req.user.role)) return next()

    return response.error(res, 'Forbidden', 403)
  }
}

export default roleAccessMiddleware