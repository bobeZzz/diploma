
export const isAdminOrManager = (role) => ['admin', 'manager'].includes(role)
