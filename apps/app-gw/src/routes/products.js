import express from 'express'

import {
  createProduct,
  updateProduct,
  getProducts,
  deleteProduct
} from '../controllers/products'
import roleAccessMiddleware from '../helpers/roleAccessMiddleware'
import validator from '../validators/products'

const router = express.Router()

router.route('/products').post(
  roleAccessMiddleware('manager'),
  validator.createProduct,
  createProduct
)

router.route('/products').delete(
  roleAccessMiddleware('manager'),
  validator.deleteProduct,
  deleteProduct
)

router.route('/products/:uuid').patch(
  roleAccessMiddleware('manager'),
  validator.updateProduct,
  updateProduct
)

router.route('/products').get(
  validator.getProducts,
  getProducts
)

export default router
