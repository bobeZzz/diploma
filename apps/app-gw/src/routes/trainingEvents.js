import express from 'express'

import trainingEventsValidator from '../validators/trainingEvents'

import {
  createTrainingEvent,
  getTrainingEvents,
  declineTrainingEvent,
  //declineTrainingEvent
} from '../controllers/trainingEvents'
import roleAccessMiddleware from '../helpers/roleAccessMiddleware'

const router = express.Router()

// manager
router.route('/trainingEvents').post(
  trainingEventsValidator.create,
  createTrainingEvent
)

router.route('/trainingEvents').get(
  trainingEventsValidator.get,
  getTrainingEvents
)

router.route('/trainingEvents/decline').post(
  trainingEventsValidator.declineTrainingEvent,
  declineTrainingEvent
)

export default router
