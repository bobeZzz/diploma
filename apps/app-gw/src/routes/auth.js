import express from 'express'

import {
  getUsers,
  getUser,
  createUser,
  getCurrentUser,
  updateOwnProfile,
  updateUser
} from '../controllers/auth'
import roleAccessMiddleware from '../helpers/roleAccessMiddleware'
import validator from '../validators/auth'

const router = express.Router()

router.route('/users').get(
  validator.getUsers,
  getUsers
)

router.route('/user').post(
  roleAccessMiddleware('manager'),
  validator.createUser,
  createUser
)

router.route('/user/:uuid').patch(
  roleAccessMiddleware('manager'),
  validator.updateUser,
  updateUser
)

router.route('/user').get(
  validator.getUser,
  getUser
)

router.route('/currentUser').get(
  validator.getCurrentUser,
  getCurrentUser
)

router.route('/updateOwnProfile').post(
  validator.updateOwnProfile,
  updateOwnProfile
)

export default router
