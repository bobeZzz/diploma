import express from 'express'

import {
  getActiveGymSessions,
  registerUserGymSession,
  closeUserGymSession
} from '../controllers/gymSessions'
import roleAccessMiddleware from '../helpers/roleAccessMiddleware'
import validator from '../validators/gymSessions'

const router = express.Router()

router.route('/gymSessions/active').get(
  roleAccessMiddleware('manager'),
  validator.getActiveGymSessions,
  getActiveGymSessions
)

router.route('/gymSessions/register').post(
  roleAccessMiddleware('manager'),
  validator.registerGymSession,
  registerUserGymSession
)

router.route('/gymSessions/close').post(
  roleAccessMiddleware('manager'),
  validator.closeGymSession,
  closeUserGymSession
)

export default router
