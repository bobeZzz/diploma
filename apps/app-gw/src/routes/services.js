import express from 'express'

import {
  createService,
  updateService,
  deleteService,
  getServices,
  updatePersonalTrainingService
} from '../controllers/services'
import roleAccessMiddleware from '../helpers/roleAccessMiddleware'
import validator from '../validators/services'

const router = express.Router()

router.route('/services').post(
  roleAccessMiddleware('manager'),
  validator.createService,
  createService
)

router.route('/services/:uuid').delete(
  roleAccessMiddleware('manager'),
  validator.deleteService,
  deleteService
)

router.route('/services/:uuid').patch(
  roleAccessMiddleware('manager'),
  validator.updateService,
  updateService
)

router.route('/services').get(
  validator.getServices,
  getServices
)

router.route('/services/personalTraining').patch(
  roleAccessMiddleware('manager'),
  validator.updateService,
  updatePersonalTrainingService
)

export default router
